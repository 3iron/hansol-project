<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.io.PrintWriter"%>




<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<!-- 뷰포트 -->
<meta name="viewport" content="width=device-width" initial-scale="1">
<!-- 스타일시트 참조 -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- summernote  -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
	integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
	crossorigin="anonymous"></script>

<link
	href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>



    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
  
  
<title>스터디 만들기 페이지</title>




</head>
<body>
	<%-- 	<!-- 로그인된 사용자라면 userID라는 변수에 해당 아이디가 담기고 그렇지 않으면 null값 211029 08:57 -->
	<%
	String userID = null;
	if (session.getAttribute("userID") != null) {
		userID = (String) session.getAttribute("userID");
	}
	%>

	<!-- 로그인된 사용자가 아니라면 로그인창으로 돌려줌 -->
	<%
	if (session.getAttribute("userID") == null) {
		session.setAttribute("loginMsg", "게시물 작성은<br>로그인이 필요합니다.");
		response.sendRedirect("/login");
		return;
	}
	%> --%>
	<!-- Core theme CSS (includes Bootstrap)-->
	<link href="css/styles.css" rel="stylesheet" />
	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>

	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container px-5">
			<a class="navbar-brand" href="mainPage">StudyMoa</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about.html">마이페이지</a></li>
					<li class="nav-item"><a class="nav-link" href="contact.html">새
							스터디만들기</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Header-->
	<header class="bg-dark py-5">
		<div class="container px-5">
			<div class="row gx-5 align-items-center justify-content-center">
				<div class="col-xl-8 col-xl-7 col-xxl-6">
					<div class="my-5 text-center text-xl-start">
						<h1 class="display-15 fw-bolder text-white mb-2"> 스터디를
							만들어보세요</h1>
						<p class="lead fw-normal text-white-50 mb-4"></p>
						<div
							class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">


						</div>
					</div>
				</div>
				<div class="col-xl-5 col-xxl-6 d-none d-xl-block text-center">
					<img class="img-fluid rounded-3 my-5" src="/img/mainPageImage.png"
						alt="..." />
				</div>
			</div>
		</div>
	</header>

	<!-- 게시판 -->

	<form class="makeStudy" action="createStudy" method="get"
		name="createStudy">

		<!-- 01 스터디 제목 S_TITLE -->
		<div class="form-group">
			<label for="title">Title</label> <input type="text"
				placeholder="스터디 제목을 입력해주세요" class="form-control" id="title"
				name="sTitle">		
		</div>
		
		<!-- 02 스터디 내용 S_CONTENT -->
		<div class="form-group">
			<label for="content">Content</label>
			<textarea class="form-control summernote" placeholder="스터디 내용을 입력해주세요" rows="5" id="sContent"
				name="sContent"></textarea>
		</div>
		
		<!--  03 스터디 카테고리 CATEGORY_NO -->
		<label for="category">Category</label>
		<div
			class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
			<select class="form-select" aria-label="Default select example"
				id="selectCategory">
				<option selected value="0">전체보기</option>
				<c:forEach items="${categoryList}" var="categoryList">
					<option value="${categoryList.categoryNo}">${categoryList.categoryName}</option>
				</c:forEach>

			</select> <a class="btn btn-primary btn-lg px-4 me-sm-3" href="#features"
				id="filterBtn">Select</a>
		</div>

		<!-- 04 참여 인원 S_TOTAL -->
<div class="form-group">
  <label for="total">참여 인원 </label>
  <input type="text" class="form-control" id="usr" name="sTotal">
</div>

		<button type="submit" id="makeStudy" class="btn btn-primary">스터디
			생성</button>
		<button id="btn-save" class="btn btn-dark text-white">닫기</button>

	</form>


	<!-- Footer-->
	<footer class="bg-dark py-4 mt-auto">
		<div class="container px-5">
			<div
				class="row align-items-center justify-content-between flex-column flex-sm-row">
				<div class="col-auto">
					<div class="small m-0 text-white">Copyright &copy; Your
						Website 2021</div>
				</div>
				<div class="col-auto">
					<a class="link-light small" href="#!">Privacy</a> <span
						class="text-white mx-1">&middot;</span> <a
						class="link-light small" href="#!">Terms</a> <span
						class="text-white mx-1">&middot;</span> <a
						class="link-light small" href="#!">Contact</a>
				</div>
			</div>
		</div>
	</footer>

</body>

<script>
	$('.summernote').summernote(
			{
				placeholder : 'Hello stand alone ui',
				tabsize : 2,
				height : 120,
				toolbar : [ [ 'style', [ 'style' ] ],
						[ 'font', [ 'bold', 'underline', 'clear' ] ],
						[ 'color', [ 'color' ] ],
						[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
						[ 'table', [ 'table' ] ],
						[ 'insert', [ 'link', 'picture', 'video' ] ],
						[ 'view', [ 'fullscreen', 'codeview', 'help' ] ] ]
			});
</script>


</html>