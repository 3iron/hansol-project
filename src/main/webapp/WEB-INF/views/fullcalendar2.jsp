<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<% String cal_no = (String) session.getAttribute("CAL_NO"); %>


<!DOCTYPE html>
<html>
<head>

<meta charset='utf-8' />
<!-- 00 fullcalendar -->
<link rel='stylesheet' href='css/fullcalendar.css' />
<script src='js/Calendarmain.js'></script>
<!-- add event 추가 -->
<meta name="_csrf" id="_csrf" content="${_csrf.token}" />
<!-- ajax 쓰기 위한 csrf 토큰 설정 -->
<meta name="_csrf_header" id="_csrf_header" content="${_csrf.headerName}" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" type="text/javascript"></script>

<!--  moment를 위한 cdn -->
<script class="cssdesk" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.0/moment.min.js" type="text/javascript"></script>

<!-- 한글을 위한 (성공) -->
<script src='fullcalendar/core/locales/ko.js'></script>


<!-- alertify style -->

<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>

</head>


<body>




	<!-- 00 기본 캘린더 -->
	<div id='calendar'></div>


	<!-- 01 일정 조회 - DB 연동 성공 -->
	<script>

	//ajax를 위한 csrf토큰 설정
	var fc_lang = "ko";
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
/* 	console.log("token+header : " + token + ", " + header); */
	
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	})
		



  document.addEventListener('DOMContentLoaded', function(){
  		
	  var calNo = 0;
	 
	  
	  $.ajax({
			
			url : "/selectFullCalendar"
		   ,contentType : "application/json"
		   ,dataType : "json"
		   ,type : "post"
		   ,beforeSend : function(xhr) { 
			 	xhr.setRequestHeader(header, token);
			}
		   ,async : false
		   ,cache : false
		   ,success : function(data){
			   
			   var calendarEl = document.getElementById('calendar');

			    var calendar = new FullCalendar.Calendar(calendarEl, {
			    	

		
			    	headerToolbar: {
			        left: 'prev,next today addEventButton',
			        center: 'title',
			       right: 'dayGridMonth,listMonth' 
			      },
			     
			   
				  locale:"ko",
	    		  editable: true,
	    		  droppable: true, 
	    	      drop: function(arg) {
	    	        if (document.getElementById('drop-remove').checked) {
	    	          arg.draggedEl.parentNode.removeChild(arg.draggedEl);
	    	        }
	    	      },

				  customButtons: {
		    		    addEventButton: {
		    		      text: 'Add Event',
		    		      click: function(schedule) {
		    		    	  var title = prompt('Event Title:');

			  			        if(title=="") {
			  			        	
			  						alertify.alert("Notice","내용을 입력해주세요"); 
			  			
			  						return false;
			  					}
			  			        
			  			        
			 			        if(title==null){
						        	
						            alertify.error("일정 등록을 취소합니다");
						          
						            return false;
						          }
			  				
		    			        
		    		    	  var start = prompt('Enter a start date in YYYYMMDD format');
		    		
			  			        if(start=="") {
			  						alertify.alert("Notice", "시작일을 입력해주세요");
			  			
			  						return false;
			  					}
			  					
			  			        
			  			      if(start==null){
						        	
						            alertify.error("일정 등록을 취소합니다");
						          
						            return false;
						          }
			  			        
			  			        
					  			  if (isNaN(start)) {
					  				alertify.alert("Notice", "시작일은 숫자만 입력 가능합니다");
					  
					  				return false;
					  			}
		    			        
					  			  
				
					  			  
				  			  	if(start.length < 8 || start.length > 8) {
				  					alertify.alert("Notice", "시작일은 YYYYMMDD에 맞춰 8자로 입력하세요");
				  			
				  					return false;
				  				}
			  			
				  			
			  					
		    		    	  var end = prompt('Enter a end date in YYYYMMDD format');
		    			    
			  			        if(end=="") {
			  						alertify.alert("Notice", "종료일을 입력해주세요");
			  			
			  						return false;
			  					}
			  			        
			  			        
			  			      if(end==null){
						        	
						            alertify.error("일정 등록을 취소합니다");
						          
						            return false;
						          }
		    		    	  
			  	 			  if (isNaN(end)) {
					  				alertify.alert("Notice", "종료일은 숫자만 입력 가능합니다");
					  
					  				return false;
					  			}
		    		    	  
			  			      if(end.length < 8 || end.length > 8) {
				  					alertify.alert("Notice", "종료일은 YYYYMMDD에 맞춰 8자로 입력하세요");
				  			
				  					return false;
				  				}
		    		    	

		    		        alertify.success("새로운 일정이 생성되었습니다!");
		    		        
		    		        
		    		        
		    		        location.href='/insertCalendar?title='+title+'&start='+start+'&end='+end; 
		    		    	}
		
			   
		    		    }
		    		  },
			      
			      initialDate: '2021-12-18',
			      navLinks: true, 
			      selectable: true,
			      selectMirror: true,
			      select: function(arg) {
			        var title = prompt("Event Title:", "");
	  
			        if (title) {
			          calendar.addEvent({
			           

			        	title: title,
			            start: arg.start,
			            end: arg.end,
			            allDay: arg.allDay,

			          })
			          
			        }

			        var start = moment(arg.start).format("YYYYMMDD");
			        var end = moment(arg.end).format("YYYYMMDD");
			        
   				
			        
			        if(title=="") {
			        	
						alertify.alert("Notice", "내용을 입력해주세요");
			
						return false;
					}
			        
			        
 			        if(title==null){
			        	
			            alertify.error("일정 등록을 취소합니다");
			          
			            return false;
			          }
			  
			        calendar.unselect()
	
		
			        location.href='/insertCalendar?title='+title+'&start='+start+'&end='+end;
			        
			      },

			      eventClick: function(arg) {
			    	  
			    	  if (confirm('일정을 지우시겠습니까?')) {
			    		  
			    		  
			    		  var calNo = arg.event.id;
		
				        arg.event.remove()
				        
				         location.href = '/deleteCalendar?calNo='+calNo;

				        
				        
				        }
			    	  
			    	  
	
			    	  
			    	    alertify.error("일정 삭제를 취소합니다");

	    		        location.href='/insertCalendar?title='+title+'&start='+start+'&end='+end; 
	
			      },
			      
			      
			      
			      editable: true,
			      dayMaxEvents: true,
			      events: data,
			      eventColor: getRandomColor("${CAL_NO}")
			
			
			      
			    });

			    calendar.render();
			    
			    
		   },
		   
		   change: function(themeSystem) {
		        calendar.setOption('themeSystem', themeSystem);
		   }

		});
  });
	
	</script>





	<script>

var dynamicColors = function() {
     var r = Math.floor(Math.random() * 255);
     var g = Math.floor(Math.random() * 255);
     var b = Math.floor(Math.random() * 255);
     return "rgb(" + r + "," + g + "," + b + ")";
 };
 
</script>

<script>
getRandomColor = function(_isAlpha) {
	  let r = getRand(0, 255),
	  g = getRand(0, 255),
	  b = getRand(0, 255),
	  a = getRand(0, 10) / 10;

	  let rgb = _isAlpha ? 'rgba' : 'rgb';
	  rgb += '(' + r + ',' + g + ',' + b;
	  rgb += _isAlpha ? ',' + a + ')' : ')';

	  return rgb;

	  // Return random number from in to max
	  function getRand(min, max) {
	    if (min >= max) return false;
	    return ~~(Math.random() * (max - min + 1)) + min;
	  };
	};

</script>




</body>
</html>


