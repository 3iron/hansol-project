<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Insert title here</title>
<!-- S : HEADER.JSP -->
<%@ include file="../common/header.jsp" %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>
<!-- E : HEADER.JSP -->

<!-- S : CSS -->
<link href="css/category/categorySelect.css" rel="stylesheet">
<!-- E : CSS -->

</head>

<body>
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter" id="selectCategoryBtn">
  관심사 선택
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">카테고리선택</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="inputArea">
		<ul class="mylist">
			<c:forEach items="${categoryList}" var="categoryList" varStatus="status">
				<li class="filterBtn">
				 <input type="hidden" value="${categoryList.categoryNo}" class="selectCategory${status.index+1}">
				 <input type="checkbox" id="cb${status.index+1}" value="${categoryList.categoryNo}" value2="${categoryList.categoryName}" name="categoryNo" />  
				   <label for="cb${status.index+1}" class="labelClass">
				   <img src="${categoryList.getCategoryThumbImg()}" style="width: 50px; height: 60px;" class="rounded-circle" /></label> <br> 
				   <span class="jg-font" style="font-size: 1em; color:black;" id="categoryName${status.index+1}">${categoryList.getCategoryName()}</span>
				</li>
			</c:forEach>
		</ul>
	</div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-primary" onclick='categoryModalController(this)'>확인</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeCategoryModal">닫기</button>
      </div>
    </div>
  </div>
</div>

</body>

<!-- S : JS -->
<script src="/js/category/categorySelect.js"></script>
<!-- E : JS -->	


</html>