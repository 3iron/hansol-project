<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.io.PrintWriter"%>
    
<!DOCTYPE html>



<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/datepickermain.css">




<div class="modal fade" id="modifyStudy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">스터디 수정하기</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="modifyStudy needs-validation" novalidate="novalidate" action="modifyStudy" method="get" name="modifyStudy" autocomplete="off" id="modifyStudyForm" >
					<div class="form-group">
						<div class="form-floating mb-4">
							<input type="hidden" value="${study.getSNo()}" name="sNo">	
							<input type="hidden" value="${study.getSmNo()}" name="smNo">
							<input type="text" class="form-control" placeholder="스터디 제목을 입력해주세요."  name="sTitle"
								required="required" minlength="4" maxlength="20">
							<label for="name">Title</label>

							<div class="valid-feedback">
								<span class="badge badge-pill badge-success">SUCCESS</span>
							</div>
							<div class="invalid-feedback">
								<span class="badge badge-pill badge-danger">DISABLED</span> 스터디
								제목은 4-20자로 입력해야 합니다.
							</div>
						</div>
					
						

					<div class="mozipDiv input-group2">
						<h4>
							<span class="badge badge-secondary">모집 기간</span>
						</h4>
						<div class="mozipInputDiv input-group ">
							<div class="form-floating mb" id="datetimepicker1">	
								<input type="text" class="datepicker form-control "
									placeholder="Start date" id="mozipStart" name="recruitSdate" data-format="dd/MM/yyyy" > <label
									for="start">Start Date</label>
								      
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control end-date"
									placeholder="End date" id="mozipEnd" name="recruitEdate"> <label
									for="end">End Date</label>
							</div>

						</div>
						<!--02-2 모임 기간 -->
						<h4>
							<span class="badge badge-secondary">모임 기간</span>
						</h4>
						<div class="input-group ">
							<div class="form-floating">
								<input type="text" placeholder="Start date"
									aria-label="First name" class="form-control "
									name="classSdate" autocomplete="off" id="moimStart"> <label
									for="content">Start Date</label>
							</div>
							<div class="form-floating mb-4">
								<input type="text" placeholder="End date" aria-label="Last name"
									class="form-control end-date" name="classEdate" id="moimEnd"
									autocomplete="off"> <label for="content">End
									Date</label>
							</div>
						</div>
					</div>

					<div class="form-floating mb-4">
						<script>$('#text').val().replace(/\n/g, "<br>")</script>
						<!-- textarea, input type="text" 위에 선언해주는 코드 -->
						<input type="text" class="form-control" id="content"
							name="classLocation" placeholder="스터디를 진행할 장소를 적어주세요. ex) 홍대입구역 "
							required="required"> <label for="content">모임 장소
						</label>
						<div class="valid-feedback">
							<span class="badge badge-pill badge-success">SUCCESS</span>
						</div>
						<div class="invalid-feedback">
							<span class="badge badge-pill badge-danger">DISABLED</span> 진행장소를 정해주세요
						</div>
					</div>

					<div class="form-floating mb-4">
						<input type="text" class="form-control" id="sContent"
								name="classTime" required="required"
								placeholder="스터디를 진행할 시간을 적어주세요. ex) 11:00 ~ 17:00 "> 
								<label for="sContent">모임 시간</label>
						<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
						<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>   시간을 정해주세요</div>
					</div>

<!-- 02-5 스터디 내용 -->

					<div class="form-floating mb-3">
						<textarea class="form-control" placeholder="스터디 내용을 입력해주세요"
								style="height: 100px" id="sContent" name="sContent" required="required"
								minlength="10"></textarea>
						<label for="sContent">스터디 내용</label>
						<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
						<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>   스터디 내용은 10자 이상 입력해야 합니다.</div>
					</div>

					<div class="mb-2">
						<input type="number" min="2" max="30" value="2" class="form-control" placeholder="참여 인원은 총 몇 명인가요?" id="usr" name="sTotal"> 
							<!-- <label for="floatingSelectGrid">참여 인원</label> -->
						<div class="valid-feedback"><span class="badge badge-pill badge-success"> SUCCESS </span></div>
						<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>   불가능한 인원입니다.</div>
						<label><small> 최소 2명 - 최대 30명입니다.</small></label>
					</div>
					
					<!-- 05 생성 전 확인 버튼 -->
					<div class="col-8 mb-4">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="invalidCheck" required> 
							<label class="form-check-label" for="invalidCheck"> Agree to terms and conditions </label>
							<div class="invalid-feedback">You must agree before submitting.</div>
						</div>
					</div>
					
					
			</div>
			<div class="modal-footer">
        			<button type="submit" id="modifyStudyBtn" class="btn btn-primary text-white">스터디 변경</button>
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary text-white">취소</button>
      </div>	
        </form>
      </div>
      
      
    </div>
  </div>
</div>  

<script src="js/makeStudy/makeStudy.js"></script>
<script src="js/datepicker.js"></script> 

<script>
	$(function() {
		$('#sTitle').keyup(function(e) {
			var content = $(this).val();
			$(this).height(((content.split('\n').length + 1) * 1.5) + 'em');
			$('#counter').html(content.length + '/300');
		});
		$('#sTitle').keyup();
	});
	
	$(".datepicker").click(function(){
			
			console.log("하이");
		
			$('#start').css("z-index","99999");
		
	});
	


	$(document).on('hidden.bs.modal', function (event) {

		if ($('.modal:visible').length) {

			$('body').addClass('modal-open');

		}

	});

</script>