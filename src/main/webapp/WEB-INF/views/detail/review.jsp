<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<meta name="_csrf" content="${_csrf.token}">
<meta name="_csrf_header" content="${_csrf.headerName}">

<!-- S : CSS -->
<link href="css/detail/review.css" rel="stylesheet">


<!-- E : CSS -->
</head>

<body> 
<div class="modal fade" id="reviewModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">스터디 종료</h5>
       
      </div>
      <div class="modal-body">
        <label class="col-form-label">스터디를 완료했습니다. 후기를 남겨주세요</label>
        <form>
          <div class="mb-3">
           <h5 class="fw-bolder">후기</h5>
            <textarea class="form-control" id="reviewText"></textarea>
          </div>
           <label for="message-text" class="col-form-label">평점</label>
           <br>
			<div class="starpoint_wrap">
			  <div class="starpoint_box">
			    <label for="starpoint_1" class="label_star" title="0.5"><span class="blind">0.5점</span></label>
			    <label for="starpoint_2" class="label_star" title="1"><span class="blind">1점</span></label>
			    <label for="starpoint_3" class="label_star" title="1.5"><span class="blind">1.5점</span></label>
			    <label for="starpoint_4" class="label_star" title="2"><span class="blind">2점</span></label>
			    <label for="starpoint_5" class="label_star" title="2.5"><span class="blind">2.5점</span></label>
			    <label for="starpoint_6" class="label_star" title="3"><span class="blind">3점</span></label>
			    <label for="starpoint_7" class="label_star" title="3.5"><span class="blind">3.5점</span></label>
			    <label for="starpoint_8" class="label_star" title="4"><span class="blind">4점</span></label>
			    <label for="starpoint_9" class="label_star" title="4.5"><span class="blind">4.5점</span></label>
			    <label for="starpoint_10" class="label_star" title="5"><span class="blind">5점</span></label>
			    <input type="radio" name="starpoint" id="starpoint_1" class="star_radio" value="0.5">
			    <input type="radio" name="starpoint" id="starpoint_2" class="star_radio" value="1">
			    <input type="radio" name="starpoint" id="starpoint_3" class="star_radio" value="1.5">
			    <input type="radio" name="starpoint" id="starpoint_4" class="star_radio" value="2.0">
			    <input type="radio" name="starpoint" id="starpoint_5" class="star_radio" value="2.5">
			    <input type="radio" name="starpoint" id="starpoint_6" class="star_radio" value="3.0">
			    <input type="radio" name="starpoint" id="starpoint_7" class="star_radio" value="3.5">
			    <input type="radio" name="starpoint" id="starpoint_8" class="star_radio" value="4.0">
			    <input type="radio" name="starpoint" id="starpoint_9" class="star_radio" value="4.5">
			    <input type="radio" name="starpoint" id="starpoint_10" class="star_radio" value="5">
			    <span class="starpoint_bg"></span>
			  </div>
			</div>
				
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">닫기</button>
        <button type="button" class="btn btn-primary" id="insertReview">작성완료</button>
      </div>
    </div>
  </div>
</div>

</body>	
<!-- S : JS -->
<!-- <script src="js/detailPage.js"></script> -->
<!-- E : JS -->
<script>


 

</script>
</html>