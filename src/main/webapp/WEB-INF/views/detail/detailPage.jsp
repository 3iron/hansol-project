<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>

<title>세상의 모든 스터디 StudyMoa</title>

<!-- S : HEADER.JSP -->

<!-- E : HEADER.JSP -->

<!-- S : CSS -->
<!-- <link href="css/detail/detailPage.css"> -->
<link href="css/styles.css" rel="stylesheet">

<!-- <link href="css/mainImageAnimation.css" rel="stylesheet"> -->
<!-- <link href="css/mainPage/mainPage.css" rel="stylesheet"> -->
<link href="css/common.css" rel="stylesheet">
<link href="css/detailPage.css" rel="stylesheet">
<link href="css/detail/grade.css" rel="stylesheet">
<!-- E : CSS -->


</head>

<body>
	<main class="flex-shrink-0" id="mainBody">
	<!-- S : Navigation-->
	<%@ include file="../common/navbar.jsp" %>
	<!-- E : Navigation-->
	
	<!-- Product section-->
	<section class="py" >
	  <div class="card o-hidden border-0">
		<div class="container px-4 px-lg-5 my-4" id="sectionBody">
			<div class="row gx-4 gx-lg-5 align-items-center rounded-3" style="width:1600px; left: -260px; position: relative;">
				<div id="imageCard" class="imageCard col-md-5" style="margin-right: 200px;">
					<div class="card h-100" style="margin-right:50px;">
						<c:if test="${study.getSEndDate() != null}">
							<div class="detailCorner-ribbon top-right sticky red">스터디종료</div>
						</c:if>
						<div class="badge bg-dark text-white position-absolute"
							style="top: 1rem; right: 1rem" id="nowPerTotal">${study.getSNow()}명 / ${study.getSTotal()}명</div>
						<img class="card-img-top mb-5 mb-md-0" src="${study.getCategoryThumbImg()}" style=" height:400px; alt="..." />
					</div>
				</div>
				<div class="explain col-md-6 mx-3">
					<div class="small mb-3 text-primary"><i class="bi bi-card-list"></i> ${category.categoryName}</div>
					<input type="hidden" value="${study.getSmNo()}" id="studySmNo" /> <input
						type="hidden" value="${study.getSTotal()}" id="studySTotal" /> <input
						type="hidden" value="${study.getSNow()}" id="studySNow" /> <input
						type="hidden" value="${study.getSNo()}" id="studySNo" />
					<input type="hidden" value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo}" id="userNo"/>
					<input type="hidden" value="${studyMemberYn.SM_NO}" id="userSmNo"/>
					
					<div class="separator"></div>
					
					<h1 class="display-5 fw-bolder ">${study.getSTitle()}</h1>
					<div class="fs-5 mb-3 text-success ">
						<span><i class="bi bi-award"></i>${study.getUserName()} </span>
					</div>
					<div class="star-ratings" style="font-size: -webkit-xxx-large;">
						<div class="star-ratings-fill space-x-2 text-lg"  id="divBGrade">
							<span class="starIcon">★</span><span>★</span><span>★</span><span>★</span><span>★</span>
						</div>
						<div class="star-ratings-base space-x-2 text-lg">
							<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
						</div>
					</div>
					<div class="separator"></div>
					<div class="" style="">
						<table class="card-table" style="position: relative !important;">
							<tbody>
								<tr class="card-table border-bottom"><th>모집기간</th><td>${study.getRecruitSdate()}~${study.getRecruitEdate()}</td></tr>
								<tr class="card-table"><th>모임기간</th><td>${study.getClassSdate()}~${study.getClassEdate()}</td></tr>
								<tr class="card-table"><th>장소</th><td>${study.getClassLocation()}</td></tr>
								<tr class="card-table border-bottom"><th>시간</th><td>${study.getClassTime()}</td></tr>
								<tr class="card-table border-bottom"><th>스터디 내용</th><td>${study.getSContent()}</td></tr>
							</tbody>
						</table>
					</div>
					
					
						
						
					<div class="joinButtonGroup" >
						
						<c:if test="${study.getSEndDate() == null}">
								<button class="btn btn-secondary flex-shrink-0" type="button" id="btnQuitStudy">
									<i class="bi bi-door-open"></i> 리뷰등록하기
								</button>
							
							<button type="button" class="btn btn-dark" id="liveToastBtn">
								<svg  xmlns="http://www.w3.org/2000/svg" width="25" height="30" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
		  							<path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
		 						    <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
		  							<path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z" />
								</svg>
							</button>
							<button type="button" class="btn btn-darkcyan" data-bs-toggle="modal" data-bs-target="#roomModal" id="goChat" style="width: 50px;">
								<img src="/img/chat-box.svg"/>
							</button>
						</c:if>
									<c:if test="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo == study.getUserNo()}">
										<c:choose>
											<c:when test="${study.getSEndDate() == null}">
												<button class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
												aria-controls="collapseExample">스터디 관리</button>
											</c:when>
											<c:otherwise>
													<button class="btn btn-warning flex-shrink-0" type="button" id="btnEndStudy" disabled>종료된 스터디입니다.</button>
											</c:otherwise>
										</c:choose>
										<div class="collapse" id="collapseExample">
											<div class="card card-body">
												<div class="btn-group" role="group" aria-label="Basic mixed styles example">
													<button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modifyStudy">수정하기</button>
										    		<!-- <button type="button" class="btn btn-outline-danger" id="deleteStudy">삭제하기</button> -->
													<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg" id="deleteStudy">삭제하기</button>-->
													<!-- <button type="button" id="myModalUse" class="btn btn-info btn-md" data-dismiss="modal">삭제하기</button> -->
													<button type="button" class="btn btn-outline-info" data-bs-toggle="modal" data-bs-target="#reviewModal" id="endStudy">종료하기</button>
												</div>
											</div>
										</div>
									</c:if>
						</div>
							
					
						<div class="justify-content-center align-items-center " style="z-index: 11" id="btnShowStudyMember">
						  <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
						    <div class="toast-header">
						     <i class="bi bi-map"></i>
						      <strong class="me-auto">&nbsp; 현재 참가자</strong>
						      <small>11 mins ago</small>
						      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
						    </div>
						    <div class="toast-body">
						    </div>
						  </div>
						  
						</div>
						<div class="">
							<%@include file="../websocket/room.jsp" %>
						</div>
					
					
					<div class="join">
						<c:choose>
							<c:when test="${study.getSEndDate() != null}">
								<p>
									<button class="btn btn-warning flex-shrink-0" type="button" id="btnEndStudy" disabled>종료된 스터디입니다.</button>
								</p>
							</c:when>
							<c:when test="${studyMemberYn.SM_NO != null && study.getUserNo() != sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo}">
								<button class="btn btn-success flex-shrink-0" type="button" id="btnJoinStudy" disabled>
									<i class="bi bi-book"></i> 신청완료
								</button>
							</c:when>
							<c:otherwise>
								<c:if test="${study.getUserNo() != sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo }">	
									<button class="btn btn-primary flex-shrink-0" type="button"
										id="btnJoinStudy">
										<i class="bi bi-book"></i> 신청하기
									</button>
								</c:if>
							</c:otherwise>
						</c:choose>
					</div>
					
				</div>
				
				
				
				
				
			</div>
		   </div>
		</div>
	</section>
	<section>
		<div class="container mw-1600px-i">
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<button class="nav-link active" id="nav-review-tab"
						data-bs-toggle="tab" data-bs-target="#nav-home" type="button"
						role="tab" aria-controls="nav-home" aria-selected="true">댓글보기</button>
					<button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
						data-bs-target="#nav-profile" type="button" role="tab"
						aria-controls="nav-profile" aria-selected="false">리뷰보기</button>
					<button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
						data-bs-target="#nav-contact" type="button" role="tab"
						aria-controls="nav-contact" aria-selected="false">관련 스터디</button>
				</div>
			</nav>
			
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active mb-4" id="nav-home" role="tabpanel"aria-labelledby="nav-home-tab">
					<div>
						<input type="text" class="inputComment" name="reply_content" id="reply" placeholder="댓글을 작성해주세요." aria-label="Recipient's username" aria-describedby="btnReply" style="margin-top:15px;">
						<button type="button" class="my-2 btn btn-outline-secondary" id="btnReply" type="button">댓글쓰기</button>
					</div>
					<input type="hidden" class="loginUserSession" value="<sec:authentication property='principal.userNo'/>"> 	
					<table class="table mb-5 text-center" id="commentTable">
						<thead class="table-secondary">
							<tr>
								<th class="col-md-1" scope="col">#</th>
								<th class="col-md-2" scope="col">Name</th>
								<th class="col-md-5" scope="col">Reply</th>
								<th class="col-md-1" scope="col">Date</th>
								<th class="col-md-2" scope="col">Modify</th>
								<th class="col-md-2" scope="col">Delete</th>
							</tr>
						</thead>
						<tbody id="replyContent" class="table-striped">
							<c:forEach items="${studyCommentList}" var="studyCommentList"
								varStatus="status">
								<tr>
									<td scope="row"><input type="hidden"
										value="${studyCommentList.SC_NO}"
										id="studyCommentNo${status.index+1}">${status.index+1}</td>
									<td id="commentUserName">${studyCommentList.USER_NAME}</td>
									<td id="commentContentList${status.index+1}">
									<input type="text" readonly="readonly" value="${studyCommentList.SC_CONTENT}" style="border: none"
										class="commentInput" id="commentScContent${status.index+1}">
									</td>
									<td id="commentScWriteDate">${studyCommentList.SC_WRITE_DATE}</td>
									<c:choose>
									<c:when test="${studyCommentList.USER_NO == sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo }">
									<td id="btnModifyContainer${status.index+1}"><input type="button"
										class="modifyComment btn-sm btn-outline-warning d-inline-block"
										value="수정" id="btnModfiyComment${status.index+1}"></td>
									</c:when>
									<c:otherwise>
										<td></td>
									</c:otherwise>
									</c:choose>
									
									<c:choose>
									<c:when test="${studyCommentList.USER_NO == sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo  }">
									<td id="btnDeleteContainer${status.index+1}"><input
										type="button"
										class="deleteComment btn-sm btn-outline-danger d-inline-block"
										value="삭제" id="btnDeleteComment${status.index+1}"></td>	
									</c:when>
									<c:otherwise >
									<td></td>
									</c:otherwise>	
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
				<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
					<table class="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th scope="col">리뷰번호</th>
								<th scope="col">리뷰일자</th>
								<th scope="col">리뷰내용</th>
								<th scope="col">리뷰평점</th>
								<th scope="col">작성자</th>
							</tr>
						</thead>
						<tbody id="reviewTbody"></tbody>
					</table>
				</div>
				
				<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" >
					<section class="py-5 bg-light">
						<div class="container px-4 px-lg-5 mt-5">
							<h2 class="relatedStudyH2 fw-bolder mb-4">Related Study</h2>
							<div
								class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
								<c:forEach items="${studyListPaging}" var="studyListPaging">
									<c:if test="${studyListPaging.getSNo() != study.getSNo()}">
										<div class="col-lg-4 mb-5">
											<div class="card h-100 overflow-auto" onClick="location.href='detailPage?sNo=${studyListPaging.getSNo()}&smNo=${studyListPaging.getSmNo()}'" style="cursor: pointer;">
												<!-- Sale badge-->
												<div class="badge bg-dark text-white position-absolute"
													style="top: 0.5rem; right: 0.5rem">${studyListPaging.getSNow()}명
													/ ${studyListPaging.getSTotal()}명</div>
												<!-- Product image-->
												<img class="relatedImg card-img-top"
													src="${studyListPaging.getCategoryThumbImg()}"
													alt="..." />
												<!-- Product details-->
												<div class="card-body">
													<div class="text-center" style="margin-top: 5px;">
														<!-- Product name-->
														<p class="fw-bolder">${studyListPaging.getSTitle()}</p>
														<!-- Product price-->
													</div>
												</div>
												<!-- Product actions-->
												<div class=" bg-transparent" style="position: relative; bottom: 10px; " >
													<h4 class="display-13 fw-bold">평점</h4>
													<div class="star-ratings">
														<div class="star-ratings-fill space-x-2 text-lg"  id="RelateCategoryDivBGrade" style="width : ${studyListPaging.getBGrade()/5 * 100}%">
															<span class="starIcon">★</span><span>★</span><span>★</span><span>★</span><span>★</span>
														</div>
														<div class="star-ratings-base space-x-2 text-lg">
															<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
														</div>
													</div>
													
												</div>
											</div>
										</div>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section>

	<!-- Related items section-->
	</main>
	
	<!-- S: Footer-->
	<%@ include file="../common/footer.jsp"%>
	<!-- E: Footer-->
	
</body>

<!-- S :리뷰 모달창 -->
<%@include file="review.jsp"%>
<!-- E :리뷰 모달창 -->
<!-- S : 수정하기 모달창 -->
<%@include file="modifyStudy.jsp" %>
<!-- E : 수정하기 모달창 -->


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">확인 사항</h4>
				<button type="button" class="close" data-miss="modal">&times;</button>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

</script>
<!-- S : JS -->
<script src="/js/detailPage.js"></script>
<!-- E : JS -->
<script src="js/chatRoom.js"></script>
<script>


$("#btnJoinStudy").click(function() {

	var studySNo = $("#studySNo").val();
	var studySTotal = $("#studySTotal").val();
	var studySNow = $("#studySNow").val();
	var studySmNo = $("#studySmNo").val();

	var study = {
		"studySNo" : studySNo,
		"studySTotal" : studySTotal,
		"studySNow" : studySNow,
		"studySmNo" : studySmNo
	}
	
	console.log("ajax 통신 시작.." + JSON.stringify(study));

	$.ajax({
		url : "/insertStudyMember",
		type : "post",
		contentType : "application/json",
		dataType : "json",
		traditional : true,
		data : JSON.stringify(study),
		success : function(data) {
			console.log(data);

			var btnJoinStudy = $("#btnJoinStudy");

			if (parseInt(data.snow) >= parseInt(data.stotal)) {

				
				$("#btnJoinStudy").addClass("btn btn-danger flex-shrink-0");
				//$(".btn btn-danger flex-shrink-0").attr('id','mozipMagam');
				$("#btnJoinStudy").text("모집마감");
				$("#btnJoinStudy").attr("disabled", true);

				
				//alert("신청완료");
				
				var html = "<br><button type='button' class='btn btn-success flex-shrink-0 mt-1' disabled>신청완료</button>";
				
				$(".joinButtonGroup").append(html);
				
			}else{
			
			btnJoinStudy.addClass("btn btn-success flex-shrink-0 mt-1");
			btnJoinStudy.text("신청완료");
			btnJoinStudy.attr("disabled", true);
			}
			
			$("#nowPerTotal").text(data.snow + "명 / " + data.stotal + "명");
			$("#btnQuitStudy").show();
		},
		error : function() {

			alert("신청오류");
		}

	});

});


$("#divBGrade").attr('style',"width : "+${study.getBGrade()}+"%");

</script>
</html>