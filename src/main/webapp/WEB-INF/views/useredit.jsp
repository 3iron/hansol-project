<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!-- S : HEADER.JSP -->
<%@ include file="common/header.jsp" %>
<!DOCTYPE html>
<html>
<!-- 로그인 페이지를 구현하기 위해 대시보드에서 제공하는 login.html이다. 김민정 -->

<!-- 1. element: <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> 
2. sb-admin-2.min.css 
3. 이미지 소스파일 : https://source.unsplash.com/K4mSJ7kc0As/600x800 -->

<head>
	<meta charset="UTF-8">
	<%@ include file="common/header.jsp" %>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<meta name="_csrf" id="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" id="_csrf_header" content="${_csrf.headerName}" />
	
	<!-- 제이쿼리를 쓰기위한 스크립트 -->
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	
	<!-- Core theme CSS (includes Bootstrap)-->
	<link href="css/styles.css" rel="stylesheet" />
	<link href="css/common.css" rel="stylesheet" />
	<!-- 1028 17:30 비밀번호 입력하지 않을시 수정 불가능 박민정 -->
</head>

<body class="d-flex flex-column h-100">
	
	<%@ include file="common/navbar.jsp"%>
	<main class="flex-shrink-0">
	
	<!-- Header -->
	<div class="mainbar bg-dark py-3" style="background: url(/img/visual2.png);background-color:#deebe9">
		<div class="container px-5" style="padding-left: 600px !important;max-width: 1600px;">
			<div class="row gx-5 align-items-center justify-content-center">
				<div class="col-xl-8 col-xl-7 col-xxl-6">
					<div class="title my-5 text-center text-xl-start" style="margin-top: 180px !important;">
						<h1 class="display-15 fw-bolder mb-2">스터디의 모든 것을 관리하세요</h1>
					<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Blog preview section-->
	<section class="">
		<!-- Contact form-->
		<div class="container px-5">
		<div class="rounded-3 py-5 px-4 px-md-5 mb-5">  <!-- 회색배경 -->
		<div class="text-center mb-5">
	
				<div class="btn-group mb-3">
		
				<!--  가입한 스터디 보기 -->	
				<a class="fs-5 px-2 link" href="/myPage" data-toggle="tooltip" data-placement="top" title="내 스터디">
				<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-journal-bookmark-fill"></i>
				</div>				
				</a>	

				<!--  회원정보수정 -->
				<a class="fs-5 px-2 link"  data-toggle="tooltip" data-placement="top" title="회원정보 수정" >
				<div class="feature bg-secondary bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-person-fill" ></i>
				</div>
				</a>			
					
				<!--  캘린더 -->
				<a class="fs-5 px-2 link-dark" href="/showCalendar" data-toggle="tooltip" data-placement="top" title="캘린더">
				<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-calendar-check-fill"></i>
				</div>	
				</a>			
					
				<!--  새로운 스터디 만들기 -->	
				<a class="fs-5 px-2 link-dark" href="/makeStudy" data-toggle="tooltip" data-placement="top" title="스터디 만들기">
				<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-journal-plus"></i>
				</div>	
				</a>
	
	</div>

	
				<h1 class="fw-bolder">회원정보 수정</h1>
					<p>내 정보를 수정할 수 있어요 </p>
		</div>


	
	 <div class="o-hidden border-0 my-5 mb-3"><br>
	 <div class="container px-5">	
	 <div class="row gx-5">


<!--  include editUser -->
<%@ include file="usereditContent.jsp" %>

	</div>
	</div>
	</section>
	</main>


		<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	
	</script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>



</body>

	<script>
	$(document).ready(function(){
		
		
		$("#editBtn").attr("disabled",true);
		//밑에 주석달기
		
		
		
		 $("#userChangePwd2").blur(function(){							//비밀번호 둘이 비교 -> 일치하면 disalbe :false, 일치하지 않으면 disalbe : true
	
				var userChangePwd = $("#userChangePwd").val();
				var userChangePwd2 = $("#userChangePwd2").val();
				
				console.log("비밀번호 비교 진입");
				console.log("userChangePwd :"+userChangePwd+"userChangePwd2 :"+userChangePwd2);
				if(userChangePwd != userChangePwd2) {
					console.log("비밀번호 비교 진입2");
					alert("비밀번호가 일치하지 않습니다.");
					$("#editBtn").attr("disabled",true);
					
				}else if(userChangePwd != "" && userChangePwd2 != "" && userChangePwd == userChangePwd2){
					alert("비밀번호가 일치합니다");
					$("#editBtn").attr("disabled",false);
				}
					
				});
		
	 	 $("#editBtn").click(function(){
	
	 			//document.querySelector("#usereditForm").addEventListener('submit', function(e) {
	 	        //e.preventDefault();
	 		 
			var userId = $("#userId").val();
			var userChangePwd = $("#userChangePwd").val();
			var userName = $("#inputName").val();
			var userPhone = $("#inputPhone").val();
			
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			
			console.log("token+header : " + token + ", " + header);						//토큰확인
	
			$(document).ajaxSend(function(e, xhr, options) {
				xhr.setRequestHeader(header, token);
			});
			
			var paramData = {														//회원정보 수정에 필요한 정보
					
					"userId" : userId,
					"userChangePwd" : userChangePwd,
					"userName" : userName,
					"userPhone" : userPhone
			};
			
			console.log("회원정보수정시 들어가는 값 jsp단 :"+JSON.stringify(paramData));
			
				$.ajax({
					url:"${pageContext.request.contextPath}/usereditForm"
				   ,type:"post"
				   ,dataType:"json"
				   ,contentType: 'application/json'
				   ,data:JSON.stringify(paramData)
				   ,beforeSend : function(xhr) { 
						xhr.setRequestHeader(header, token);
					}

				   ,cache : false
				   ,success:function(data){
					   if(data == true){												//회워정보가 수정된 경우
						   console.log("jsp단 data의 결과값: "+data);
						   Swal.fire({
							   title: "회원정보가 수정되었습니다",
							   text: "수정한 회원정보로 로그인하시기 바랍니다.",
							   icon: "success",	
							   buttons: "로그인 화면으로 돌아가기",
							  
							 }).then((result) =>{
									location.href='/login'; 
								 })
							 }
							 
						   

					   else{															//회원정보가 수정되지 않은경우
						   Swal.fire({
							   title: "회원정보가 수정되지 않았습니다!",
							   text: "입력하신 정보를 다시 확인해주시기 바랍니다.",
							   icon: "warning",
							   buttons: "아이디 다시찾기",
							  
							 });
					   
					   
					   }
				   },
				   error : function(){
					   alert("통신실패");
				   }

				});	
																				//ajax
	
		}); 
		
	 	 
	 	 $("#deleteuser").click(function(){
	 		 /* 
		 		document.querySelector("#usereditForm").addEventListener('submit', function(e) {
		 	        e.preventDefault();

		 	      
		 	    }); */
		 		 
				var userNo = $("#userNo").val();
				
				var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
		
				console.log("token+header : " + token + ", " + header);						//토큰확인
		
				$(document).ajaxSend(function(e, xhr, options) {
					xhr.setRequestHeader(header, token);
				});
				
				
				var paramData = {														//회원정보 수정에 필요한 정보
						
						"userNo" : userNo
				};
				
				
				Swal.fire({
					  title: "회원정보를 삭제하시겠습니까?",
					  text: "한번 삭제된 정보는 복구되지 않습니다",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
					})
					.then((willDelete) => {
					  if (willDelete) {
					    
						  
						  $.ajax({
								url:"${pageContext.request.contextPath}/deleteuser"
							   ,type:"post"
							   ,dataType:"json"
							   ,contentType: 'application/json'
							   ,data:JSON.stringify(paramData)
							   ,beforeSend : function(xhr) { 
									xhr.setRequestHeader(header, token);
								}

							   ,cache : false
							   ,success:function(data){
								   if(data = true){
									   console.log("로그인 삭제완료");
									   Swal.fire("회원정보가 삭제되었습니다.", {
										      icon: "success",
										    });
									   setTimeout(function() {
										   location.href="login";
										 }, 2000);
									   
								   }
								   else{
									   console.log("로그인 삭제실패");
									   Swal.fire("회원정보가 삭제되지 않았습니다", {
										      icon: "warning",
										    });
								   }
							   },
							   error : function(){
								   alert("통신실패");
							   }

							});							//ajax				
						  
						  
					  } 
					  
					  else {
						  Swal.fire("취소되었습니다.");
					  }
					});
		 		 
																	//ajax
				
				
				
		 	 });
		
	});
	</script>
	
	

	
	
</html>