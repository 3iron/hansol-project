<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  
<!DOCTYPE html>
<html lang="en">
<!-- 로그인 페이지를 구현하기 위해 대시보드에서 제공하는 login.html이다. 김민정 -->

<!-- 1. element: <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> 
2. sb-admin-2.min.css 
3. 이미지 소스파일 : https://source.unsplash.com/K4mSJ7kc0As/600x800 -->

<head>

    <title>StudyMoa LOGIN PAGE</title>
	<%@ include file="common/header.jsp"%>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
</head>

<body class="bg-dark">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5" style="border:solid 0px !important;padding:0px !important;">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row" style="padding-bottom:0px !important;">
                            <div class="loginImage col-lg-6 d-none d-lg-block"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">
                                        스터디 할 사람 여기로
                                        </h1>
                                        <p class="mainText">STUDYMOA</p>
                                    </div>
                                    <!-- action은 도메인 주소명시값을 따라간다. [2021.10.29 김민정] -->
                                    
                                    <form class="user" action="/login" id="userLoginForm" method="post">
                                        <div class="form-group">
                                          
                                            <span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">아이디</span>
                                            <input type="text" class="form-control form-control-user"
                                                id="username" name="username" aria-describedby="emailHelp"
                                                placeholder="계정 ID" >
                                        </div>
                                       
                                        <div class="form-group">
                                         <span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">패스워드</span>
                                            <input type="password" class="form-control form-control-user"
                                                id="password" name="password" placeholder="패스워드" >
                                        </div>
                                       
                                        <!-- <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="loginCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                       
                                        </div> -->
                                      
                                      	<!-- <a th:href="@{/mainPage}" class="btn btn-primary btn-user btn-block" onclick="loginClick()"> -->
                                      	<input class="btn btn-primary btn-user btn-block" type="submit" value="Login" id="loginBtn">
                                      	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                      	<!-- a태그에서는 type="submit"속성이 먹히지 않는다 [2021.10.29 김민정] -->

                                       <!--  </a> -->
                                        <hr>
                                       <!-- <a href="mainPage.jsp" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> JOIN US!
                                        </a> --> 
                                        
                                        
                                    </form>
                                     
                                    <div class="text-center">
                                        <a class="small" href="join">회원가입</a>
                                    </div>
                                    
                                    <hr>
                                    
                                    <div class="text-center">
                                        <a class="small" href="findid">아이디 찾기</a>
                                    </div>
                                    
                                    <div class="text-center">
                                        <a class="small" href="findpw">비밀번호 찾기</a>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>