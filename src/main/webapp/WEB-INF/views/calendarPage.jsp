<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
	<!-- Core theme JS-->
	<script src="js/scripts.js"></script>
	<!-- Core theme CSS (includes Bootstrap)-->
	<link href="css/styles.css" rel="stylesheet" />
	<link href="css/common.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
	
	<%@ include file="common/navbar.jsp"%>
	
	<main class="flex-shrink-0">
	
	<!-- Header -->
	<div class="mainbar bg-dark py-3" style="background: url(/img/visual2.png);background-color:#deebe9">
		<div class="container px-5" style="padding-left: 600px !important;max-width: 1600px;">
			<div class="row gx-5 align-items-center justify-content-center">
				<div class="col-xl-8 col-xl-7 col-xxl-6">
					<div class="title my-5 text-center text-xl-start" style="margin-top: 180px !important;">
						<h1 class="display-15 fw-bolder mb-2">스터디의 모든 것을 관리하세요</h1>
					<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!--  달력 소환 -->
	<!-- Blog preview section-->
	<section class="">
		<!-- Contact form-->
		<div class="container px-5">
			<!-- Contact form-->
			<div class="rounded-3 py-5 px-4 px-md-5 mb-5">  <!-- 회색배경 -->
				<div class="text-center mb-5">
		
		
		
		
		<div class="btn-group mb-3">
		
		
				<!--  가입한 스터디 보기 -->	
				<a class="fs-5 px-2 link"  href="/myPage" data-toggle="tooltip" data-placement="top" title="내 스터디">
									<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
			<i class="bi bi-journal-bookmark-fill"></i>
					</div>				

</a>	


	
		<!--  회원정보수정 -->
		<a class="fs-5 px-2 link" href="/useredit" data-toggle="tooltip" data-placement="top" title="회원정보 수정" >
					<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-person-fill" ></i>
				</div>
					</a>			
					
				<!--  캘린더 -->
				
	<a class="fs-5 px-2 link-dark"  data-toggle="tooltip" data-placement="top" title="캘린더">
					<div class="feature bg-secondary bg-gradient text-white rounded-3 mb-3">
					<i class="bi bi-calendar-check-fill"></i>
					</div>	
			</a>			
					
					<!--  새로운 스터디 만들기 -->
					
				<a class="fs-5 px-2 link-dark" href="/makeStudy" data-toggle="tooltip" data-placement="top" title="스터디 만들기">
								<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
					<i class="bi bi-journal-plus"></i>
					</div>	
		</a>
	
	</div>
				
					<h1 class="fw-bolder">캘린더</h1>
							<p>일정을 관리할 수 있어요 </p>
				
				</div>
				
				
		
		<div class="o-hidden border-0 my-1">
	<div class="bg-white rounded-3 py-5 px-4 px-md-5 mb-5">  <!-- 흰색배경 -->				
					<div class="calendar">
					<!--  include문으로 full calendar 붙이기 -->
 	<%@ include file="fullcalendar2.jsp" %> 
	</div>						
	</div>
</div>
	</div>
	
	
		<!--  가입한 스터디가 없을 경우 : 아직은 조회페이지 만드는 중이라 not empty로 줌  -->

		<c:if test="${not empty categoryInfo}">
			<c:set var="cart" value="false" />
			<h2>${userInfo.userName}님은가입한스터디가 없습니다</h2>
			<div class="fw-bolder">
				<p class="card-text">관심있는 스터디를 찾아보세요</p>
			</div>
		</c:if>


		<!-- 가입된 스터디가 있을 경우 / 가입된 스터디 조회하기 -->

	

	
		</section>
	</main>


		<!-- Footer-->
		<%@ include file="common/footer.jsp" %>
		
	</body>
		
	<script src="js/myPage.js"></script>
	
		<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	
	</script>
	
	
</html>