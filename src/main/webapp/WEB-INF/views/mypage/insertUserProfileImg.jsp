<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


</head>
<body>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">	
	<div class="modal-content">
	  <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">프로필 사진 추가</h5>
      </div>
       <div class="modal-body">
	 <form action="./insertProfileImg?${_csrf.parameterName}=${_csrf.token}" method="POST" enctype="multipart/form-data" name="readForm" autocomplete="off">
		<div class="inputArea">
		 <label for="gdsImg">이미지</label>
		 <input type="file" id="gdsImg" name="file" accept=".jpg, .png"/>
		 <div class="select_img"><img src="" /></div>
		</div>
		
		<div class="inputArea">
			<button type="submit" id="register_Btn" class="btn btn-primary">등록</button>
		</div>
	 </form>
	 </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">닫기</button>
        <button type="button" class="btn btn-primary" id="insertProfileImg">작성완료</button>
      </div>
	</div>
  </div>
</div>	
	
</body>

<script src="js/insertUserProfileImg.js"></script>

</html>