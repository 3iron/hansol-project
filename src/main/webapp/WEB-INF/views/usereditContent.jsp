<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="EUC-KR"%>

<!-- Nested Row within Card Body -->
<!-- <form class="user" action="/usereditForm"  method="post" > -->
<div class="group row" style="height: 300px;">
	<input type="hidden" name="userOrgPwd" value="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userPwd}" />
	<input type="hidden" value="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userNo}" id="userNo"/>
	<div style="display:block;">
		<table class="card-table">
			<tbody>
				<tr class="border-bottom">
					<th><span id="userId" class="badgeCommon" >ID</span></th>
					<td>${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userId}</td>
					
					<th><span class="badgeCommon">비밀번호</span></th>
					<td>
						<input type="text" id="userChangePwd" name="userChangePwd" class="form-control form-control-user" placeholder="변경할 비밀번호" required>
						<input type="text" id="userChangePwd2" name="userChangePwd2" class="form-control form-control-user" placeholder="비밀번호 확인" required>
					</td>
				</tr>
				<tr class="border-bottom">
					<th><span class="badgeCommon">이름</span></th>
					<td>
						<input id="inputName" name="userName" class="form-control form-control-user" value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userName}" disabled>
<!-- 						<button type="button" class="modifyEditUser btn btn-light" id="userNameBtn">수정</button> -->
					</td>
					
					<th><span class="badgeCommon">전화번호</span></th>
					<td>
						<input id="inputPhone" name="userPhone" class="form-control form-control-user" value="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userPhone}" disabled >
<!-- 						<button type="button" class="modifyEditUser btn btn-light mx-2" id="editPhoneBtn">수정</button> -->
					</td>
					
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="d-grid gap-2 d-md-flex justify-content-md-end">

<button type="submit" class="btn btn-dark text-white btn-user" id="editBtn" name="editBtn">회원정보수정</button>

<button type="button" class="btn btn-dark nav-link" id="deleteuser" name="deleteuser">회원 탈퇴</button>
<input type="reset" value="취소" class="btn btn-outline-secondary text-dark btn-user" >

</div>
<!-- </form> -->

<script>
	var flag1 = false;
	$("#editPhoneBtn").click(function(){
		
		console.log("수정버튼");
		if(flag1 == false){
			
			$("#inputPhone").attr('disabled',false);
			$(this).html("확인");
			flag1 = true;
		}else{
			
			$("#inputPhone").attr('disabled',true);
			$(this).html("수정");
			flag1 = false;
		}
		
		
	});
	var flag2 = false;
	
	$("#userNameBtn").click(function(){
		
		if(flag2 == false){
			
			$("#inputName").attr('disabled',false);
			$(this).html("확인");
			flag2 = true;
		}else{
			
			$("#inputName").attr('disabled',true);
			$(this).html("수정");
			flag2 = false;
		}
		
		
	});
</script>