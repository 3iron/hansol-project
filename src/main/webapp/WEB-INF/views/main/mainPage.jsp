<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>

<head>

<title>세상의 모든 스터디 StudyMoa</title>

<%@ include file="../common/header.jsp" %>

<!-- S: CSS -->
<link href="css/styles.css" rel="stylesheet">
<link href="css/icons.css" rel="stylesheet">
<link href="css/mainImageAnimation.css" rel="stylesheet">
<link href="css/mainPage/mainPage.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">

</head>

<body class="d-flex flex-column h-100">
	<main class="flex-shrink-0" id="mainBody">
		<!-- S: Navigation-->
		 <%@ include file="../common/navbar.jsp"%> 
		<!-- E: Navigation-->
		
		<!-- Header-->
		<input type="hidden"  id="loginUserNo" value="<sec:authentication property='principal.userNo'/>">
		<input type="hidden" id="sessionId" value="">
		<div class="mainbar bg-gold" style="background: url(/img/visual1.png);">
			<div id="container" class="container">
				<div class="row">
					<div class="col-xl-8 col-xl-7 col-xxl-6">
						<div class="my-5 text-left text-xl-start" style="margin-top:120px !important;">
							<h1 class="display-15 fw-bolder mb-2">여러분의 스터디를 찾아보세요</h1>
							<p class="lead fw-normal mb-4">현재 등록된 스터디는 ${studySize}개 입니다.</p>
							<!-- <img class="img-fluid rounded-3 my-5" src="/img/mainPageImage.png" style="width:200px; height:200px" alt="..." /> -->
						</div>
						
						<div class="" style="width:600px;">
							<div class="input-group mb-2">
								<input class="form-control inputSearch" type="text" placeholder=""
									aria-label="" aria-describedby="button-newsletter" onkeyup="enterkey()" id="searchKeyWord"/>
								<button class="btn btn-outline-light" id="searchKeyWordBtn" type="button" style="background-color: #70cacd;">검색하기</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Blog preview section-->
		<section class="sectionBody py-5">
			<div class="container px-5">
				
				<div class="catListWrapper">
					<ul class="myList">
						
						<li class="filterBtn">
							<div onClick="location.href='/mainPage';">
								<input type="hidden" value="" class="selectCategory"> 
								<span class="jg-font" style="font-size: 35px;font-weight:600;color:#343a40!important;">ALL</span>
								 <div style="height:10px;" ></div>
								<span class="jg-font" style="font-size: 20px;font-weight:600;color:#343a40!important;">전체보기</span>
							</div>
						</li>
						
						<c:forEach items="${categoryList}" var="categoryList" varStatus="status">
						<li class="filterBtn">
							<div onClick="location.href='/mainPageCategoryResult?categoryNo=${categoryList.categoryNo}'">
								<input type="hidden" value="${categoryList.categoryNo}" class="selectCategory${status.index+1}"> 
								<img src="${categoryList.getCategoryThumbImg()}" style="width: 60px; height: 60px;" />
								<div style="height:10px;"></div>
								<span class="jg-font" style="font-size: 20px;font-weight:600;color:#343a40!important;">${categoryList.getCategoryName()}</span>
							</div>
						</li>
						</c:forEach>
						
					</ul>
				</div>
				
				<div class="row gx-5 py-5 justify-content-center">
					<div class="col-lg-8 col-xl-6">
						<div class="text-center">
							<c:choose>
								<c:when test="${not empty categoryInfo}">
									<h2 class="fw-bolder">${categoryInfo.categoryName}</h2>
								</c:when>
								<c:otherwise>
									<h2 class="fw-bolder">전체보기</h2>
								</c:otherwise>
							</c:choose>
							<p class="" style="font-size: 20px;">다양한 종류의 스터디를 한 곳에서 만나보세요.</p>
						</div>
					</div>
				</div>
				
				<div class="col gx-5" >
					<c:forEach var="list" items="${list}">
						<div class="row mb-4"  id="goDetailStudy"  onclick="location.href='detailPage?sNo=${list.getSNo()}&smNo=${list.getSmNo()}'" style="cursor:pointer">
							<div class="mainCard card h-100 bg-white">
								
								<div style="float:left;padding-top:5px;">
									<figure class="snip1273">
										<c:if test="${list.getSEndDate() != null}">
											<div class="corner-ribbon top-right sticky red">스터디종료</div>
										</c:if>
										<img class="card-img-top" src="${list.getCategoryThumbImg()}" alt="..."  />		<!-- 카테고리별 이미지 -->
									</figure>
								</div>
								<div id="titleMark" class="contents" style="float:left;">
									<p class="jg-font title">
										<a class="text-decoration-none link-black" href="${pageContext.request.contextPath}/detailPage?sNo=${list.getSNo()}&smNo=${list.getSmNo()}">${list.getSTitle()}</a>
									</p>
									<div class="separator"></div>
									<table class="card-table">
										<tr class="card-table border-bottom"><th class="recruit">모집기간</th><td>${list.getRecruitSdate()}~${list.getRecruitEdate()}</td></tr>
										<tr class="card-table"><th class="recruit">모임기간</th><td>${list.getClassSdate()}~${list.getClassEdate()}</td></tr>
										<tr class="card-table"><th class="recruit">장소</th><td>${list.getClassLocation()}</td></tr>
										<tr class="card-table"><th class="recruit">시간</th><td>${list.getClassTime()}</td></tr>
									</table>
								</div>
								<div>
								<input type="hidden" name="smNo" id="studySmNo" value="${list.getSmNo()}">
	 							<input type="hidden" name="sNo" id="studySNo" value="${list.getSNo()}">
	 							
								</div>
							</div>
						</div>
					</c:forEach>
					
					<!-- 페이징 include 처리 -->
					<%@ include file="../common/pagination.jsp"%>
					<!-- Call to action-->
<!-- 					<aside class="bg-secondary bg-dark rounded-3 p-4 p-sm-5 mt-5"> -->
<!-- 						<div -->
<!-- 							class="d-flex align-items-center justify-content-between flex-column flex-xl-row text-center text-xl-start"> -->
<!-- 							<div class="mb-4 mb-xl-0"> -->
<!-- 								<div class="fs-3 fw-bold text-white">주제 및 제목 검색</div> -->
<!-- 								<div class="text-white-50">원하시는 주제 혹은 제목을 검색해보세요</div> -->
<!-- 							</div> -->
<!-- 							<div class="ms-xl-4"> -->
<!-- 								<div class="input-group mb-2"> -->
<!-- 									<input class="form-control" type="text" placeholder="" -->
<!-- 										aria-label="" aria-describedby="button-newsletter" onkeyup="enterkey()" id="searchKeyWord"/> -->
<!-- 									<button class="btn btn-outline-light" id="searchKeyWordBtn" -->
<!-- 										type="button">검색하기</button> -->
<!-- 								</div> -->
<!-- 								<div class="small text-white-50">We care about privacy, -->
<!-- 									and will never share your data.</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</aside> -->
				</div>
			</div>	
		</section>
	</main>
	
	<!-- S: Footer-->
	<%@ include file="../common/footer.jsp"%>
	<!-- E: Footer-->
	
</body>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/mainPage.js"></script>

</html>