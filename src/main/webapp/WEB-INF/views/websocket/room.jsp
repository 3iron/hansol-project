<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>


</head>
<body>

	<div class="modal fade" id="roomModal" tabindex="-1"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div
			class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">스터디 채팅</h5>
				</div>
				<div class="modal-body">
					<div class="container">
						<h1>채팅방</h1>
						<div id="roomContainer" class="roomContainer">
							<table id="roomList" class="roomList"></table>
						</div>
						<div>
							<table class="inputTable">
								<tr>
									<th>방제목</th>
									<th><input type="text" name="roomName" id="roomName"></th>
									<th><button id="createRoom">방만들기</button></th>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-bs-dismiss="modal">닫기</button>
					<button type="button" class="btn btn-primary" id="insertReview">작성완료</button>
				</div>
			</div>
		</div>
	</div>
	 
</body>

</html>