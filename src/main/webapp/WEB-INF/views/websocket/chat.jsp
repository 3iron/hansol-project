<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!-- S:CSS -->
<link href="css/websocket/chat.css" rel="stylesheet">
<!-- E:CSS -->
<%@ include file="../common/header.jsp" %>
</head>
<script src="js/websocket/chat.js"></script>

<body>

	<h1>채팅</h1>
		<div class="row">
			<input type="hidden" id="sessionId" value="">

			<!-- <div  class="chating" id="chating"> -->
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" id="chating">
					<div class="card ml-5" id="messageBox">
						<div class="card-body height3">
						<div class="card-header my-3">Chat</div>
							<ul class="chat-list">
							</ul>
						</div>

						
					</div>
				</div>
			<!-- </div> -->
		</div>
		<div class="row inputContent">
			<div id="yourName">
			<input type="hidden" value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userProfileImg}" id="userImg">
				<table class="inputTable" style="    margin-left: 230px;">
					<tr>
					 	<th><label for="">사용자명</label></th>
   					 	<th><input type="text" class="form-control input-lg" name="userName" id="userName" aria-describedby="" placeholder="사용자명 입력"></th>
						<th><button onclick="chatName()" id="startBtn" class="btn btn-warning flex-shrink-0">이름등록</button></th>
						
					</tr>
				</table>
			</div>
		</div>
		<div class="row inputName">
			<div id="yourMsg" >
				<table class="inputTable" style="margin-left: 80px;">
					<tr>
						<th><label for="exampleInputEmail1">Message</label></th>
						<th><input id="chatting" class="form-control"  aria-describedby="" placeholder="메세지 입력"></th>
						<th><button onclick="send()" id="sendBtn" class="btn btn-primary flex-shrink-0">보내기</button></th>
					</tr>
				</table>
			</div>
		</div>
	
  

</body>
</html>