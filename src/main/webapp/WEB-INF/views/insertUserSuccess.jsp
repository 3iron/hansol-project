<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Error</title>
</head>
<body>

<!-- link href에 실제 spring boot의 구조 -->

<link href="/css/font-awesome.min.css" rel="stylesheet">
<div class="error-header"> </div>
<div class="container ">
    <section class="error-container text-center">
        <h1>ATTENTION</h1>
        
        <div class="error-divider">
            <h2>회원가입이 완료되었습니다.</h2>
            <p class="description">Success Join</p>
        </div>
        <a href="/login" class="return-btn"><i class="fa fa-home"></i>로그인창으로 돌아가기</a>
    </section>
</div>
</body>
</html>