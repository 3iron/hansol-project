var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");

		console.log("token+header : " + token + ", " + header);

		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		})
        
        
        var webSocket;
						  		  
		  function send(){
			    //var type ;
				var target = $('#receiverUserNo').val();
				var content = $('.modal-body textarea').val();
				var url = '/selectAlarm';
			
				console.log("target: "+target+", content : "+content);
				
				var paramData = {"type" : "message",
								 "target" : target, 
								 "content" : content, 
								 "url" : url};
			
				
				webSocket.send(JSON.stringify(paramData));
				//$("#exampleFormControlTextarea1").val("");
				$("#exampleFormControlTextarea1").val('');
				$("#exampleModal").modal('hide');
				
		  }
			
		function webSocketOpen(){
			webSocket = new WebSocket("ws://" + location.host + "/alarm");
			console.log(webSocket);
			webSocketEvt();			
			}
			
			
			function webSocketEvt(){
				console.log("이벤트시작");
				webSocket.onopen = function(data){
					
					
				}
				
				webSocket.onmessage = function(data){
					
					var msg = data.data;
					
					console.log("message정보 : "+msg);
					
					if(msg != null && msg.trim() != ""){
						//var d = JSON.parse(msg);
						//console.log("d : "+d);
					if(msg.type == "getId"){
							var sId = msg.sessionId != null ? msg.sessionId : "";
							console.log("sId : "+sId);
							if(sId != ""){
								$("#sessionId").val(sId);
								
								
							}
					}else if(msg.type == "message"){
					
					}	
				  }				
					
				}
										
					
			  }
							
		
			
			  
        
      
           
            
  			$.ajax({
  					
  				url : "/selectInsertUserChart"				//최근 일주일 내 가입자수 조회
  			   ,type : "post"
  			   ,dataType : "json"
 			   ,contentType : "application/json"
 				  ,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
 						xhr.setRequestHeader(header, token);
 					},
 					async : false,
 					cache : false
 			   ,success : function(data){	 
 				   
 				 var linechart = document.getElementById('lineChart').getContext('2d');				   
				
 				 var labels = [];
 				 
 				 var cnt = [];
 				
 				 for(var i=0 in data){
 					 labels.push(data[i].date);
 					 cnt.push(data[i].cnt);
 				 }
 				 
 				 
 				 var myChart = new Chart(linechart, {
 	            	type : 'line',
 	            	data : {
 	            		labels : labels,
 	            		datasets: [{
 	            			label : '최근 일주일 내 가입현황',
 	            			data : cnt,
 	            			fill: false,
 	            		    borderColor: 'rgb(75, 192, 192)',
 	            		    tension: 0.1,
 	            			borderWidth: 1
 	            		}]
 	            	},
 	            	options : {
 	            		scales : {
 	            			y : {
 	            				beginAtZero : true	
 	            			}
 	            		}
 	            	}
 	            });
 	            
 				   
 			   },error : function(){
 				   alert("ajax 통신 에러");
 			   }
  				
  				
  			});	
        	
			$.ajax({
					
				  url : "/selectCategoryDistribution"			//카테고리별 분포 조회	
				 ,type : "post"
			  	 ,dataType : "json"
			 	 ,contentType : "application/json"
			 	 ,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			 		xhr.setRequestHeader(header, token);
			 	 }
			 	 ,async : false
			 	 ,cache : false
			 	 ,success : function(data){		
				
			 		 var doughnutChart = document.getElementById('doughnutChart').getContext('2d');			
			 		 
			 		 var labels = [];
	 				 
	 				 var cnt = [];
	 				 
	 				 var color = [];
	 				 
	 				 //var rgb = ['yellow','blue','green','purple','red'];
	 				 
	 				 var dynamicColors = function() {
	 			        var r = Math.floor(Math.random() * 255);
	 			        var g = Math.floor(Math.random() * 255);
	 			        var b = Math.floor(Math.random() * 255);
	 			        return "rgb(" + r + "," + g + "," + b + ")";
	 			    };

	 				 for(var i=0 in data){
	 					 labels.push(data[i].categoryName);
	 					 cnt.push(data[i].cnt);
						 color.push(dynamicColors());
	 				 }
	 				 
	 				  
	 				 
	 				 
			 		 	console.log("color : "+color);
			 		 	
	 				 var myChart = new Chart(doughnutChart, {
	  	            	type : 'doughnut',
	  	            	data : {
	  	            		labels : labels,
	  	            		datasets: [{
	  	            			label : '카테고리별 분포 현황',
	  	            			data : cnt,
	  	            			fill: false,
	  	            		    borderColor: 'rgb(75, 192, 192)',
	  	            		    tension: 0.1,
	  	            			borderWidth: 1,
	  	            			backgroundColor : color 
	  	            		}],
	  	            		
	  	            	},
	  	            	options : {
	  	            		scales : {
	  	            			y : {
	  	            				beginAtZero : true	
	  	            			}
	  	            		}
	  	            	}
	  	            });
	 				 
			 	 }
			 	 ,error : function(){
			 		 alert("ajax 카테고리별 분포 조회 실패");
			 	 }
			 		 
				});
  
    
				var userFlag = false;
				
				$.ajax({
					
					
					url : "/selectUserInfo"
				   ,type : "post"
				   ,dataType: "json"
				   ,contentType : "application/json"
				   ,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
					 	xhr.setRequestHeader(header, token);
					}
				   ,async : false
				   ,cache : false
				   ,success : function(data){
					   if(userFlag == false){
					   for(var i=0 in data){
					   $("#userTable").append($("<tr class='right'>"+"<th>"+data[i].USER_NO+"</th>"
							   						  +"<th>"+data[i].USER_ID+"</th>"
							   						  +"<th>"+data[i].USER_NAME+"</th>"
							   						  +"<th>"+data[i].USER_PHONE+"</th>"
							   						  +"<th>"+data[i].INST_DATE+"</th>"
							   						  +"<th>"+data[i].STATUS+"</th>"
							   						  +"</tr>"));
					    }
					  }
					   userFlag = true;
				   }
				   ,error : function(){
						alert("유저정보 조회 통신 실패");   
				   }	
				});
			
			$(function(){						//유저 우클릭 이벤트
							
				$.contextMenu({							
			        selector: '.right',
			        trigger: 'right',
			        callback: function(key, options) {
			            var m = "clicked: " + key;
			           
	        			
			            //window.console && console.log(m) || alert(m);
			        },
			        items: {
			        	"user" : {name : "유저정보", callback : function(key, options){
			        			var tr = $(this);
			        			var th = tr.children();
			        		
			        			//alert(th.eq(0).text());
			        		
			        		}
			        	},
			        	"sep1": "---------",
			            "warning": {name : "메세지", icon : "fas fa-exclamation-circle", callback : function(key, options){
								

						  		var tr = $(this);
		        				var th = tr.children();
		        			
		        				var userNo = th.eq(0).text();
			            		
		        				$("#receiverUserNo").val(userNo);
			            		//alert("메세지보내기");
			            		webSocketOpen();
			            		$("#exampleModal").modal('show');
			            		
								
								//var modal = $('.modal-content').has(e.target);
								
							
								
								
								
			            }},
			            "resign": {name : "탈퇴", icon : "fas fa-minus-circle" , callback : function(key, options){
							
			            	var tr = $(this);
		        			var th = tr.children();
		        			
		        			var userNo = th.eq(0).text();
							
		        			var paramData = { "userNo" : userNo};
							
		        			Swal.fire({
		        				  title: '회원을 탈퇴시키겠습니까?',
		        				  text: "회원정보는 복구되지 않습니다.",
		        				  icon: 'warning',
		        				  showCancelButton: true,
		        				  confirmButtonColor: '#3085d6',
		        				  cancelButtonColor: '#d33',
		        				  confirmButtonText: '삭제'
		        				}).then((result) => {
		        				if (result.isConfirmed) {
		        					Swal.fire(
		        						      'Deleted!',
		        						      '삭제되었습니다.',
		        						      'success'
		        						    )
		        				
		        			$.ajax({
							
								url : "/deleteUser"					//유저 강퇴기능
								,type : "post"
								,dataType: "json"
								,contentType : "application/json"
								,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
									xhr.setRequestHeader(header, token);
								 }
								,data : JSON.stringify(paramData)
								,async : false
								,cache : false
								,success : function(data){
									
									location.reload();
									
								},error : function(){
									alert("회원탈퇴 통신 실패");
								}
								
								});
		        		 	 }
		        		  });		
						}},
			            "sep1": "---------",
			            "quit": {name : "Quit", icon : function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
			        }
			    });

	
		});	
		
		$.ajax({
			 url : "/selectLoginUser"
			,type : "post"
			,dataType: "json"
			,contentType : "application/json"
			,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			 }
			,data : JSON.stringify(loginUser)
			,success : function(data){
				
				location.reload();
				
			},error : function(){
				alert("회원탈퇴 통신 실패");
			}
			
		});