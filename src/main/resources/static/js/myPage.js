/*!
* Start Bootstrap - Modern Business v5.0.5 (https://startbootstrap.com/template-overviews/modern-business)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, token);
	})

	
	function openNav() {
	  document.getElementById("mySidenav").style.width = "250px";
	  document.getElementById("main").style.marginLeft = "250px";
	}

	function closeNav() {
	  document.getElementById("mySidenav").style.width = "0";
	  document.getElementById("main").style.marginLeft= "0";
	}
	
	$("#insertProfileImg").click(function() {

		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");

		console.log("token+header : " + token + ", " + header);

		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		})

		var srContent = $("#reviewText").val();
		var srGrade = $('input[name="starpoint"]:checked').val();
		var sNo = $("#studySNo").val();

		var review = {
			"srContent" : srContent,
			"srGrade" : srGrade,
			"sNo" : sNo
		};

		console.log(JSON.stringify(review));

		$.ajax({

			url : "/insertStudyReview",
			type : "post",
			contentType : "application/json",
			dataType : "json",
			data : JSON.stringify(review),
			beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			},
			async : false,
			cache : false,
			success : function(data) {
				alert("리뷰가 작성되었습니다.");
				$('#closeModal').click();
				location.reload();
			},
			error : function() {
				alert("실패");
			}
		})

	});