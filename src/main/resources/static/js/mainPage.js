/*!
* Start Bootstrap - Modern Business v5.0.5 (https://startbootstrap.com/template-overviews/modern-business)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});


	window.onload = function(){
		$.LoadingOverlay("show", {
			background       : "rgba(0, 0, 0, 0.5)",
			image            : "",
			maxSize          : 60,
			fontawesome      : "fa fa-spinner fa-pulse fa-fw",
			fontawesomeColor : "#FFFFFF",
		});
		
		$.LoadingOverlay("hide");
		
		console.log("하이");
		

	}




	$(".filterBtn")
			.click(
					function() {
						var rid = $(".rid").val();
						var categoryNo = $(".selectCategory"+rid).val();
						console.log("categoryNo :"+categoryNo+", rid : "+rid);
						var categoryName = $("#selectCategoryName").val();

						/* if (categoryNo != 0) {
							location.href = "${pageContext.request.contextPath}/mainPageCategoryResult?categoryNo="
									+ categoryNo;
						} else {
							location.href = "${pageContext.request.contextPath}/mainPage"
						} */
					});
	
	$("#searchKeyWordBtn").click(
		function(){
			var searchKeyWord = $("#searchKeyWord").val();
			
			location.href = "/mainPage?keyword="+searchKeyWord;
		}		
	);
	
	function enterkey(){
		
		var searchKeyWord = $("#searchKeyWord").val();
		
		if(window.event.keyCode == 13 && searchKeyWord != null){
			
			location.href = "/mainPage?keyword="+searchKeyWord;
		}
	}
	
	var coll = document.getElementsByClassName("collapsible");
	var i;

	for (i = 0; i < coll.length; i++) {
	  coll[i].addEventListener("click", function() {
	    this.classList.toggle("active");
	    var content = this.nextElementSibling;
	    if (content.style.display === "block") {
	      content.style.display = "none";
	    } else {
	      content.style.display = "block";
	    }
	  });
	}
	
	/*$("#goDetailStudy").click(function(){
	
		var sNo = $("#studySNo").val();
		var smNo = $("#studySmNo").val();
		console.log(sNo + ", "+smNo);	
		location.href="/detailPage?sNo="+sNo+"&smNo="+smNo;
		});*/