var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});


	window.onload = function(){
		$("#yourMsg").hide();
	}
	
	var webSocket;
	
	function webSocketOpen(){
		webSocket = new WebSocket("ws://" + location.host + "/chating");		
		console.log("webSocket : "+JSON.stringify(webSocket));
		webSocketEvt();
	}
	
	function webSocketEvt(){
		
		console.log("웹소켓 이벤트 열림");
		
		webSocket.onopen = function(data){
			//소켓이 열리면 초기화 세팅하기
			
		}
		
		webSocket.onmessage = function(data){
			var msg = data.data;
			var userImg = $("#userImg").val();
			//console.log("입장만해도 되나요? "+msg);
			if(msg != null && msg.trim() != ""){
				var d = JSON.parse(msg);
				
				if(d.type == "getId"){
				//$("#chating").append("<p>"+ msg + "</p>");		
					var sId = d.sessionId != null ? d.sessionId : "";
					if(sId != ""){
						$("#sessionId").val(sId);
						
						
					}
				}else if(d.type == "message"){
					if(d.sessionId == $("#sessionId").val()){
						$(".chat-list").append("<li class='out'>"
											  +"<div class='chat-img'>"
											  +"<img alt='Avtar' src='"+userImg+"'>"
											  +"</div>"
											  +"<div class='chat-body'>"
											  +"<div class='chat-message'>"
											  +"<h5>나</h5>"
											  +"<p>"+ d.msg +"</p>"
											  +"</div>"
											  +"</div>");
						/* $(".out").append("<p class='me'>나 : "+ d.msg + "</p>"); */
					}else{
						$(".chat-list").append("<li class='in'>"
											  +"<div class='chat-img'>"
											  +"<img alt='Avtar' src='https://bootdey.com/img/Content/avatar/avatar1.png'>"
											  +"</div>"
											  +"<div class='chat-body'>"
											  +"<div class='chat-message'>"
											  +"<h5>"+d.userName+"</h5>"
											  +"<p>"+ d.msg +"</p>"
											  +"</div>"
											  +"</div>");
					/* 	$(".out").append("<p class='other'>" + d.userName + " : "+ d.msg + "</p>");	 */
					}
				}else{
					console.warn("unknown type!");
				}
			}
		}
		
		document.addEventListener("keypress", function(e){
			if(e.keyCode == 13){	//enter press
				send();
			}
		});
	}
	
	function chatName(){
		var userName = $("#userName").val();
		if(userName == null || userName.trim() == ""){
			alert("사용자 이름을 입력해주세요");
			$("#userName").focus();
		}else{
			webSocketOpen();
			
			//console.log("열림");
			$("#yourName").hide();
			$("#yourMsg").show();
			
			
		}
	}
	
	function send(){
	
		var option = {
				type : "message",
		   sessionId : $("#sessionId").val(),
		    userName : $("#userName").val(),
		    	 msg : $("#chatting").val()
			}	
		webSocket.send(JSON.stringify(option))
		$("#chatting").val("");
	}
	
	