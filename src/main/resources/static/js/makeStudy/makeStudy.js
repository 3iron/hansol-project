(function (){
		  'use strict'

		  // Fetch all the forms we want to apply custom Bootstrap validation styles to
		  var forms = document.querySelectorAll('.needs-validation')

		  // Loop over them and prevent submission
		  Array.prototype.slice.call(forms)
		    .forEach(function (form) {
		      form.addEventListener('submit', function (event) {
		        if (!form.checkValidity()) {
		          event.preventDefault()
		          event.stopPropagation()
		        }

		        form.classList.add('was-validated')
		      }, false)
		    })
		})();



$("#mainPage").click(function(){

	Swal.fire({
			text:"스터디 작성을 취소하고 메인페이지로 돌아갑니다."
			
			
	}).then((result)=>{
				if(result.isConfirmed){
					
				location.href="/closeStudy";
				}
			
	
	})
	
});


$('input[type="checkbox"][name="categoryNo"]').click(function(){
	
	if($(this).prop('checked')){
		$('input[type="checkbox"][name="categoryNo"]').prop('checked',false);
		
		$(this).prop('checked', true);
	}
});

function categoryModalController(){
	
	var categoryNo = $("input:checkbox[name='categoryNo']:checked").val();
	var categoryName = $("input:checkbox[name='categoryNo']:checked").attr("value2");
	console.log("categoryNo : "+categoryNo+"categoryName : "+categoryName);
	
	if($("input:checkbox[name=categoryNo]").is(':checked') == true){
		$("#selectCategoryBtn").text(categoryName);
		$("#selectCategoryBtn").attr("class","btn btn-secondary d-flex justify-content-start");
		$("#checkboxNoLabel").attr("checked",true);	
		
	}else{
		alert("카테고리를 선택하세요!");
		return false;
	}
	
	
	$("#closeCategoryModal").click();
	$('.modal-backdrop').remove();
}



 $(function() {
      var $startDate = $('.start-date');
      var $endDate = $('.end-date');

      $startDate.datepicker({
        autoHide: true,

      });
      $endDate.datepicker({
        autoHide: true,
        startDate: $startDate.datepicker('getDate'),
        
      });

      $startDate.on('change', function () {
        $endDate.datepicker('setStartDate', $startDate.datepicker('getDate'));
      });
    });
    
    var is_empty = false;
    
    $("#makeStudy").click(function(){
		
		
		
	if($("input:checkbox[id=checkboxNoLabel]").is(":checked") == false){
			is_empty = true;
			console.log("is_empty : "+is_empty);
		}else{
			is_empty = false;
		} 
		
		//console.log($("#checkboxNoLabel").val());
		
		if(is_empty){
			alert("카테고리를 입력하세요");
			
			//return false;
		}
		
	});
	
	
	 $('#mozipStart').datepicker({
	 		
     dateFormat: 'dd-mm-yy',
     minDate: '+5d',
     changeMonth: true,
     changeYear: true,
     altField: "#mozipStart",
     altFormat: "yy-mm-dd"
 });

	 $('#mozipEnd').datepicker({
	 		
     dateFormat: 'dd-mm-yy',
     minDate: '+5d',
     changeMonth: true,
     changeYear: true,
     altField: "#mozipEnd",
     altFormat: "yy-mm-dd"
 });

	$('#moimStart').datepicker({
	 		
     dateFormat: 'dd-mm-yy',
     minDate: '+5d',
     changeMonth: true,
     changeYear: true,
     altField: "#mozipEnd",
     altFormat: "yy-mm-dd"
 });


 $('#moimEnd').datepicker({
	 		
     dateFormat: 'dd-mm-yy',
     minDate: '+5d',
     changeMonth: true,
     changeYear: true,
     altField: "#mozipEnd",
     altFormat: "yy-mm-dd"
 });
 
  /* $("#modifyStudyBtn").click(function(){
		
		
		if($("input:checkbox[id=checkboxNoLabel]").is(":checked") == false){
			is_empty = true;
			console.log("is_empty : "+is_empty);
		}else{
			is_empty = false;
		} 
		
		//console.log($("#checkboxNoLabel").val());
		
		if(is_empty){
			alert("카테고리를 입력하세요");
			
			return false;
			
		}
	});*/
	
	
	$("#modalClose").click(function(){
		$("#modifyStudy").remove();
		
	});