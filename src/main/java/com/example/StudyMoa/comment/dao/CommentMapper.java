package com.example.StudyMoa.comment.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentMapper {

	boolean insertStudyComment(HashMap<String, Object> dataParam);

	List<Map> selectCommentList(Map<String, Object> map);

	boolean updateStudyComment(HashMap<String, Object> dataParam);

	
	boolean deleteStudyComment(HashMap<String, Object> dataParam);

	HashMap<String, Object> selectStudyComment(HashMap<String, Object> dataParam);
	
}
