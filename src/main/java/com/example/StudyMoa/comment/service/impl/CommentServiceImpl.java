package com.example.StudyMoa.comment.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.comment.dao.CommentMapper;
import com.example.StudyMoa.comment.service.CommentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CommentServiceImpl implements CommentService{
	
	@Autowired
	CommentMapper commentMapper;
	
	@Override
	public boolean insertStudyComment(HashMap<String, Object> dataParam) {
		
		return commentMapper.insertStudyComment(dataParam);
	}

	@Override
	public List<Map> selectCommentList(Map<String, Object> map) {
		
		return commentMapper.selectCommentList(map);
	}

	@Override
	public boolean updateStudyComment(HashMap<String, Object> dataParam) {
	
		return commentMapper.updateStudyComment(dataParam);
	}

	@Override
	public boolean deleteStudyComment(HashMap<String, Object> dataParam) {
		
		return commentMapper.deleteStudyComment(dataParam);
	}

	@Override
	public HashMap<String, Object> selectStudyComment(HashMap<String, Object> dataParam) {
		// TODO Auto-generated method stub
		return commentMapper.selectStudyComment(dataParam);
	}

	
	
}
