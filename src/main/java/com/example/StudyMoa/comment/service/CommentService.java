package com.example.StudyMoa.comment.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CommentService {

	boolean insertStudyComment(HashMap<String, Object> dataParam);

	List<Map> selectCommentList(Map<String, Object> map);

	boolean updateStudyComment(HashMap<String, Object> dataParam);

	boolean deleteStudyComment(HashMap<String, Object> dataParam);

	HashMap<String, Object> selectStudyComment(HashMap<String, Object> dataParam);
	
}
