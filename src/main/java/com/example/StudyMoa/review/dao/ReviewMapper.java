package com.example.StudyMoa.review.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.review.dto.Review;

@Mapper
public interface ReviewMapper {

	boolean insertStudyReview(Map review);

	List<Map> selectStudyReview(HashMap<String, Object> dataParam);

	double selectReviewGrade(Map<String, Object> map);

	HashMap<String, Object> selectStudyReviewOne(HashMap<String, Object> dataParam);
			
}
