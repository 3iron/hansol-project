

package com.example.StudyMoa.review.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.StudyMemberService;
import com.example.StudyMoa.common.service.StudyService;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.review.dto.Review;
import com.example.StudyMoa.review.service.ReviewService;

@Controller
public class ReviewController {

	@Autowired
	ReviewService reviewService;
	
	@Autowired
	StudyService studyService;
	
	@Autowired
	StudyMemberService studyMemberService;
	
	@RequestMapping(value="/insertStudyReview", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean insertStudyReview(@RequestBody HashMap<String,Object> reviewParam, @AuthenticationPrincipal User loginUser) throws Exception{

		System.out.println("reviewParam : "+reviewParam);
	
		reviewParam.put("userNo", loginUser.getUserNo());	
		boolean result = reviewService.insertStudyReview(reviewParam);						//studyReview insert
		
		List<Map> review = reviewService.selectStudyReview(reviewParam);
		
		//reviewParam.put("USER_NAME", "김말자");												//나중에 삭제할 구문 (세션으로 변경)
		Study study = studyService.selectStudyOne(reviewParam);
		Map<String, Object> studyMember = studyMemberService.selectStudyMember(reviewParam);
		System.out.println("studyMember : "+studyMember);
		if(((String)studyMember.get("SM_ROLE")).equals("스터디장")){
			boolean endStudy = studyService.updateStudySEndDate(reviewParam); 					//스터디 종료일자 업데이트
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value="/selectStudyReview", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Map> selectStudyReview(@RequestBody HashMap<String,Object> dataParam, Model model){
		boolean result = true;
		
		//Map<String,Object> map = new HashMap<String,Object>();
		
		//dataParam.put("userNo", "1");
		
		//System.out.println("map 정보: "+map);
		
		List<Map> selectStudyReview = reviewService.selectStudyReview(dataParam);			//스터디 리뷰 조회
		
		model.addAttribute("selectStudyReview", selectStudyReview);
		
		return selectStudyReview;
	}
	
	@RequestMapping(value="/selectStudyReviewOne", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String,Object> selectStudyReviewOne(@RequestBody HashMap<String,Object> dataParam, @AuthenticationPrincipal User loginUser){
		
		dataParam.put("userNo", loginUser.getUserNo());
		
		Map<String, Object> studyMemberYn = studyMemberService.selectStudyMember(dataParam);					//스터디 사람인지 먼저 확인
		
		if(studyMemberYn== null){																				//스터디 멤버가 아닐 때
			HashMap<String, Object> noStudyMember = new HashMap<String,Object>();
			noStudyMember.put("SM_NO", null);
			return noStudyMember;
		}else {																									//스터디 멤버일 때
			HashMap<String,Object> selectStudyReviewOne = reviewService.selectStudyReviewOne(dataParam);			//스터디 리뷰 조회
			
			if(selectStudyReviewOne == null){
				HashMap<String, Object> selectOne = new HashMap<String,Object>();
				selectOne.put("SM_NO",studyMemberYn.get("SM_NO"));
				selectOne.put("SR_NO", null);
				selectOne.put("SR_YN", null);
				selectOne.put("SM_ROLE",studyMemberYn.get("SM_ROLE"));
				return selectOne; 
			}else {
				selectStudyReviewOne.put("SM_ROLE", studyMemberYn.get("SM_ROLE"));
				selectStudyReviewOne.put("SM_NO",studyMemberYn.get("SM_NO"));
			return selectStudyReviewOne;
			}

		}
		
				
	}
	
	
	
}
