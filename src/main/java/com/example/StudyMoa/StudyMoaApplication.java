package com.example.StudyMoa;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

//@ComponentScan(basePackages = {"com.example.StudyMoa.login.service.LoginService"})
//@ComponentScan(basePackages =  {"com.example.StudyMoa.common.utils"})

@SpringBootApplication
public class StudyMoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudyMoaApplication.class, args);
	}
	
	@Bean
	public InternalResourceViewResolver setupViewResolver(){
			InternalResourceViewResolver resolver = new InternalResourceViewResolver();
			
			resolver.setPrefix("WEB-INF/views/");
			resolver.setSuffix(".jsp");
			return resolver;
	}
	
	
	

}
