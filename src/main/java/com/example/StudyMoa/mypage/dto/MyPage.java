package com.example.StudyMoa.mypage.dto;


import java.sql.Date;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MyPage {

	
	private int sNo;
	private String sTitle;
	private String sContent;
	private Date  sWriteDate;
	private int bGrade;
	private int sTotal;
	private int sNow;
	private Date sUpdtDate;
	private int categoryNo;
	private int userNo;
	private String smRole;
	private String userName;
	private int smNo;
	
	
	@Override
	public String toString() {
		return "Study [sNo=" + sNo + ", sTitle=" + sTitle + ", sContent=" + sContent + ", sWriteDate=" + sWriteDate
				+ ", bGrade=" + bGrade + ", sTotal=" + sTotal + ", sNow=" + sNow + ", sUpdtDate=" + sUpdtDate
				+ ", categoryNo=" + categoryNo + ", userNo=" + userNo + ", smRole=" + smRole + ", userName=" + userName
				+ ", smNo=" + smNo + "]";
	}

}
