package com.example.StudyMoa.mypage.service;

import com.example.StudyMoa.login.dto.User;

public interface ProfileImgService {

	boolean insertProfileImg(User user);

}
