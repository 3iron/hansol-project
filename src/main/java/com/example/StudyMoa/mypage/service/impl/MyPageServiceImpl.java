package com.example.StudyMoa.mypage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.mypage.dao.MyPageMapper;
import com.example.StudyMoa.mypage.dto.MyPage;
import com.example.StudyMoa.mypage.service.MyPageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MyPageServiceImpl implements MyPageService {
	// MyPageMapper와 연결
	@Autowired
	MyPageMapper myPageMapper;

	// 스터디 리스트에서 내가 가입한 리스트만 볼 수 있게
	@Override
	public List<Map<String, Object>> selectMyStudyList(Map<String, Object> map) throws Exception {

		return myPageMapper.selectMyStudyList(map);
	}

	// 내가 가입한 스터디의 개수를 세기 위해
	@Override
	public int selectMyStudyCount(Map<String, Object> map) throws Exception {

		return myPageMapper.selectMyStudyCount(map);
	}

	@Override
	public boolean updateStudySNow(int sNow) {
		return myPageMapper.updateStudySNow(sNow);
	}

	@Override
	public boolean updateStudyOne(Study study) {
		return myPageMapper.updateStudyOne(study);
	}


}
