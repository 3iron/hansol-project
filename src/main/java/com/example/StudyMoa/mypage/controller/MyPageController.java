package com.example.StudyMoa.mypage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.mypage.dto.MyPage;
import com.example.StudyMoa.mypage.service.MyPageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MyPageController {
	
	
	@Autowired
	MyPageService myPageService;
	
	@Autowired
	CategoryService categoryService;
	
	// 가입한 스터디 조회 - 수정중 
	@RequestMapping(value = "/myPage")
	public String goMyPage(Model model, Paging paging
			,@RequestParam(value="nowPage", required = false) String nowPage
			,@RequestParam(value="cntPerPage", required = false) String cntPerPage
			,@RequestParam(value="keyword", required = false) String keyword
			,@AuthenticationPrincipal User loginUser) throws Exception {
		
//		List<Category> categoryList = categoryService.selectCategoryList(); 		//카테고리 리스트 조회

		Map<String, Object> map = new HashMap<String, Object>();					//조회 파라미터로 넣을 MAP 생성
	
		if(nowPage == null && cntPerPage == null){									//RequestParam값이 null일 경우 자동 현재페이지 설정
			nowPage = "1";
			cntPerPage = "3";
		}else if(nowPage == null){
			nowPage = "1";
		}else if(cntPerPage == null){
			cntPerPage = "3";
		}
//		
//		map.put("keyword", keyword);
//		
//		map.put("categoryNo", null);												//category필터링 안했기 때문에 null값 
		
		// 내 스터디가 몇개 있는지 조회
		/* map.put("userNo", 3); */												//[session구현되면 수정할 부분]
		map.put("userNo",loginUser.getUserNo());
		
		System.out.println("map : "+map);
		int total = myPageService.selectMyStudyCount(map);								//사용자의 스터디 개수 조회
		
		paging = new Paging(total, Integer.parseInt(nowPage), Integer.parseInt(cntPerPage));
		
		map.put("startPage", paging.getStart());									//시작 페이징 세팅
		map.put("endPage", paging.getCntPerPage());									//페이지 당 보여줄 스터디 갯수
//		map.put("userNo","${userNo}");													//[수정필요]사용자의 스터디만 조회 (내 스터디 조회)
		List<Map<String,Object>> mystudyListPaging = myPageService.selectMyStudyList(map);			//페이징 내 유저의 스터디 리스트 조회
		
		
		model.addAttribute("paging", paging);
		model.addAttribute("list", mystudyListPaging);
		model.addAttribute("mystudySize", total);
//		model.addAttribute("categoryList", categoryList);
		
		return "myPage";

	}

	@RequestMapping(value = "/editUserInfo")
	public String goEditUserInfo() {

		return "editUserInfo";
	}

	@RequestMapping(value = "/makeStudy")
	public String gomakeStudy(Model model) {

		return "makeStudy";

	}
	
	@RequestMapping(value="/modifyStudy")
	public String modifyStudy(Study study, RedirectAttributes redirectAttributes){
		System.out.println("sNo : "+study.getSNo());
		
				
		boolean result = myPageService.updateStudyOne(study);
		redirectAttributes.addAttribute("sNo",study.getSNo());
		redirectAttributes.addAttribute("smNo",study.getSmNo());
		
		return "redirect:/detailPage?";
	}
	
	

	

	
	
}
