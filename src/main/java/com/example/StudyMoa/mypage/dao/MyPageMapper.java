package com.example.StudyMoa.mypage.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.mypage.dto.MyPage;

@Mapper
public interface MyPageMapper {
	
//	// 내 스터디의 리스트를 보여줌
	List<Map<String, Object>> selectMyStudyList(Map<String, Object> map) throws Exception;
	
	// 내가 가입한 스터디가 몇 개인지
	int selectMyStudyCount(Map<String, Object> map) throws Exception;

	boolean updateStudySNow(int sNow);

	boolean updateStudyOne(Study study);

	
//	// 스터디 인원이 몇명인지 (가입시 1명 추가)
//	boolean updateStudySNow(int sNow);

}
