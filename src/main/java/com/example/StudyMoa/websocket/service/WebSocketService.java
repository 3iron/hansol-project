package com.example.StudyMoa.websocket.service;

import java.util.HashMap;
import java.util.List;

import com.example.StudyMoa.websocket.dto.ChatRoom;

public interface WebSocketService {

	boolean insertChatRoom(ChatRoom room);

	ChatRoom selectChatRoom(HashMap<Object, Object> params);

}
