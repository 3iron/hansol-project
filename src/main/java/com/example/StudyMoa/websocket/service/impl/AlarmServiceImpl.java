package com.example.StudyMoa.websocket.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.websocket.dao.AlarmMapper;
import com.example.StudyMoa.websocket.service.AlarmService;

@Service
public class AlarmServiceImpl implements AlarmService{

	@Autowired
	AlarmMapper alarmMapper;
	
	
	@Override
	public boolean insertAlarm(HashMap<String, Object> paramData) {
		
		return alarmMapper.insertAlarm(paramData);
	}

	
	
}
