package com.example.StudyMoa.websocket.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.websocket.dto.ChatRoom;
import com.example.StudyMoa.websocket.service.WebSocketService;

@Controller
public class WebSocketController {

	List<ChatRoom> roomList = new ArrayList<ChatRoom>();
	static int roomNumber = 0;
	
	@Autowired
	WebSocketService webSocketService;
	
	
	@RequestMapping("/chat")
	public String testChat(){
	
		return "websocket/chat";
	}
	
	
	/*방 페이지
	 * @return*/
	@RequestMapping("/room")
	public String room(){
		
		return "websocket/room";
	}
	
	/*방 생성하기
	 *@params params
	 *@return*/
	@RequestMapping(value="/createRoom", method = RequestMethod.POST)
	@ResponseBody
	public  ChatRoom createRoom(@RequestParam HashMap<Object, Object>params, @AuthenticationPrincipal User loginUser){
		String roomName = (String) params.get("roomName");
		int sNo = Integer.parseInt((String) params.get("sNo")); 
		int smNo = Integer.parseInt((String) params.get("smNo"));
		
		ChatRoom selectChatRoomOne = webSocketService.selectChatRoom(params);		//chatRoom정보 조회
		
		if(selectChatRoomOne == null){
			ChatRoom room = new ChatRoom();
			room.setRoomNumber(++roomNumber);
			room.setRoomName(roomName);
			room.setUserNo(loginUser.getUserNo());														//sessino구현 시 수정될 부분
			room.setSNo(sNo);
			room.setSmNo(smNo);
			
			
			boolean result = webSocketService.insertChatRoom(room);					//chatRoom정보 insert
			
			selectChatRoomOne = webSocketService.selectChatRoom(params);		//chatRoom정보 조회
			roomList.add(room);
			
		}
	
		
		
		return selectChatRoomOne;
	}
	
	
	//방 정보가져오기 
	@RequestMapping(value="/getRoom", method = RequestMethod.POST)
	@ResponseBody
	public  ChatRoom getRoom(@RequestParam HashMap<Object, Object> params){
		System.out.println("getRoom실행");
		
		ChatRoom selectChatRoomOne = webSocketService.selectChatRoom(params);		//chatRoom정보 조회
		if(selectChatRoomOne != null){
			System.out.println("null이아님");
			return selectChatRoomOne;
		}else {
			System.out.println("null임");
			return null;
		}
		//return selectChatRoomOne;
	}
	
	//채팅방 return
	@RequestMapping("/moveChating")
	public String chating(@RequestParam HashMap<Object,Object> params, Model model){
		int roomNumber = Integer.parseInt((String)params.get("roomNumber"));
		
		//List<ChatRoom> newList = roomList.stream().filter(o->o.getRoomNumber()==roomNumber).collect(Collectors.toList());
		
		ChatRoom chatRoom = webSocketService.selectChatRoom(params);
		
		if(chatRoom != null){
			model.addAttribute("roomName", params.get("roomName"));
			model.addAttribute("roomNumber", params.get("roomNumber"));
			return "websocket/chat";
		}else {
			return "websocket/room";
		}	
	}
	
	
	
}
