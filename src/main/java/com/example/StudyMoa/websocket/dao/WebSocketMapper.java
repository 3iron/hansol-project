package com.example.StudyMoa.websocket.dao;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.websocket.dto.ChatRoom;

@Mapper
public interface WebSocketMapper {

	boolean insertChatRoom(ChatRoom room);

	ChatRoom selectChatRoom(HashMap<Object, Object> params);

}
