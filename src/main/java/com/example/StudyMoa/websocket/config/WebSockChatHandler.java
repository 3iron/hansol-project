package com.example.StudyMoa.websocket.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("webSocketHandler")
public class WebSockChatHandler extends TextWebSocketHandler{	//서버와 클라이언트는 1:N 관계, 서버에는 여러 클라이언트가 발송한 메시지를 받아 처리해주는 Handler
	
	HashMap<String, WebSocketSession> sessionMap = new HashMap<>();	//웹소켓 세션을 담아둘 Map
	
	@SuppressWarnings("unchecked")
	@Override
	//생성된 세션 저장 -> 발신 메시지 타입 getId 명시 -> 세션ID값을 클라이언트단으로 발송
	//클라이언트 단에서는 type값을 통해 메시지와 초기 설정값을 구분할 예정
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		//소켓 연결
		super.afterConnectionEstablished(session);
		
		System.out.println("session확인 : "+session);
		
		sessionMap.put(session.getId(), session);
		JSONObject obj = new JSONObject();
		
		obj.put("type", "getId");
		obj.put("sessionId", session.getId());
		
		System.out.println("obj정보 : "+obj);
		session.sendMessage(new TextMessage(obj.toJSONString()));
		
	}
	
	
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) {
		//메시지 발송
		String msg = message.getPayload();
		
		JSONObject obj = jsonToObjectParser(msg);
		System.out.println("메시지 정보 : "+obj + ", msg : "+msg);
		for(String key: sessionMap.keySet()){
			WebSocketSession wss = sessionMap.get(key);
			
			try {
				wss.sendMessage(new TextMessage(obj.toJSONString()));
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	
	}
	

	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		//소켓 종료
		sessionMap.remove(session.getId());
		super.afterConnectionClosed(session, status);
	}
	
	
	//Handler에 JSON파일이 들어오면 파싱함. json형태의 문자열을 파라미터로 받아서 SimpleJson파서를 활용하여 JSONObject로 파싱함
	private static JSONObject jsonToObjectParser(String jsonStr){		
		JSONParser parser = new JSONParser();
		JSONObject obj = null;
		
			try {
				obj = (JSONObject) parser.parse(jsonStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
					
		return obj;		
	}
	
	
	
}
