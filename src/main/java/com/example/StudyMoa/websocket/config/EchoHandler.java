package com.example.StudyMoa.websocket.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;


@Component("echoHandler")
public class EchoHandler extends TextWebSocketHandler{

	
	//로그인 중인 개별 유저
	Map<String, WebSocketSession> users = new ConcurrentHashMap<String, WebSocketSession>();
	
	Map<String,Integer> loginUser = new HashMap<String, Integer>();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		
		super.afterConnectionEstablished(session);
		
		String userNo = getUserNo(session);
		//obj.put("userNo", 1);
		System.out.println("접속 이벤트 : "+userNo);
		//String senderId = getMemberId(session); //접속한 유저의 http세션을 조회하여 id를 얻는 함수
		
		
		if(userNo != null){	//로그인 값이 있는 경우만
			
			users.put(userNo, session);	//로그인중인 개별유저 저장
			System.out.println("로그인 값이 있는 경우만 users : "+users);
		}
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		System.out.println("관리자 메세지 보내기");
		String senderId = getUserNo(session); //접속한 유저의 http세션을 조회하여 id를 얻는 함수
		//특정 유저에게 보내기
		String msg = message.getPayload();
		System.out.println("senderId "+senderId+", msg : "+msg);
		
		if(msg != null){
				
			
				JSONObject obj = jsonToObjectParser(msg);
				String type = (String) obj.get("type");
				//String receiveSession = (String) obj.get("receiveSession");
				String target = (String) obj.get("target");
				String content = (String) obj.get("content");
				String url = (String) obj.get("url");
				
				
				WebSocketSession targetSession = users.get(target);	//메세지를 받을 세션조회
				System.out.println("type : "+type+", target : "+target+", content : "+content+", targetSession :"+targetSession);
				
				System.out.println("type :" + obj.get("type")+", targetSession : "+targetSession);
				
				//실시간 접속시
				if(targetSession != null){
					// ex : [&분의 일] 신청이 들어왔습니다.
					TextMessage tmpMsg = new TextMessage("<a target='_blank' href='"+ url +"'>[<b>" + type + "</b>] " + content + "</a>");
					System.out.println("tmpMsg : "+tmpMsg);
					targetSession.sendMessage(tmpMsg);
				}		
				
			
		}
		
	}
	
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		System.out.println(session.getId() + "익셉션 발생" + exception.getMessage());
	}
	
	//웹소켓에 id 가져오기
	//접속한 유저의 http 세션을 조회하여 id를 얻는 함수
	
	private String getMemberId(WebSocketSession session) throws IOException{
		
		JSONObject obj = new JSONObject();
		obj.put("type", "getId");
		obj.put("sessionId", session.getId());
		
		System.out.println("obj정보 : "+obj);
		session.sendMessage(new TextMessage(obj.toJSONString()));
		
		String userNo	=  (String) obj.get("sessionId");	//세션에 저장된 userNo 기준 조회
		//System.out.println("httpsession 정보 : "+httpsession+", userNo : "+userNo);
		return userNo == null? null : userNo;
	}
	
	//Handler에 JSON파일이 들어오면 파싱함. json형태의 문자열을 파라미터로 받아서 SimpleJson파서를 활용하여 JSONObject로 파싱함
	private static JSONObject jsonToObjectParser(String jsonStr){		
		JSONParser parser = new JSONParser();
		JSONObject obj = null;
		
			try {
				obj = (JSONObject) parser.parse(jsonStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
					
		return obj;		
	}
	
	private String getUserNo(WebSocketSession session){
		//System.out.println("login session정보 : "+request+", webSocketSession : "+session);
		Map<String, Object> httpsession = session.getAttributes();
		System.out.println("httpsession : "+httpsession);
		SecurityContext loginUserInfo  = (SecurityContext) httpsession.get("SPRING_SECURITY_CONTEXT");
		Authentication authentication = loginUserInfo.getAuthentication();
		User principal = (User) authentication.getPrincipal();
		String userNo = Integer.toString(principal.getUserNo());
	
		
		System.out.println("userNo : "+userNo);
		//String userNo = "";
		//Integer.toString(loginUserInfo.getUserNo());
		
		if(userNo == null){
			return session.getId();
		}else {
			return userNo;
		}
	}
	
	
}
