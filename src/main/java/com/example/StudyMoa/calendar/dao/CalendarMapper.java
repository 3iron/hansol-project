package com.example.StudyMoa.calendar.dao;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CalendarMapper {

	List<Map<String, Object>> selectCalendarList(Map<String, Object> myschedule);

	boolean insertCalendar(HashMap<String, Object> schedule);

	List<Map<String, Object>> selectCalendarListTest(Map<String, Object> myschedule);

	boolean deleteCalendar(Map<String, Object> schedule);

	


}
