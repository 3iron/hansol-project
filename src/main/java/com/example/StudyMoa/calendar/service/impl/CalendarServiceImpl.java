package com.example.StudyMoa.calendar.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.calendar.dao.CalendarMapper;
import com.example.StudyMoa.calendar.service.CalendarService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CalendarServiceImpl implements CalendarService{

	@Autowired
	CalendarMapper calendarMapper;


	public boolean insertCalendar(HashMap<String, Object> schedule) {
		
		return calendarMapper.insertCalendar(schedule);
	}

	@Override
	public List<Map<String, Object>> selectCalendarList(Map<String, Object> myschedule) {
		// TODO Auto-generated method stub
		return calendarMapper.selectCalendarList(myschedule);
	}

	@Override
	public List<Map<String, Object>> selectCalendarListTest(Map<String, Object> myschedule) {
		// TODO Auto-generated method stub
		return calendarMapper.selectCalendarListTest(myschedule);
	}

	/*
	 * @Override public boolean insertCalendar(HashMap<String, Object> schedule) {
	 * // TODO Auto-generated method stub return
	 * calendarMapper.insertCalendar(schedule); }
	 */
	// 삭제 
	@Override
	public boolean deleteCalendar(Map<String, Object> schedule) {
		return calendarMapper.deleteCalendar(schedule);
	}

	// 수정
	@Override
	public boolean updateCalendar(Map<String, Object> schedule) {
		// TODO Auto-generated method stub
		return false;
	}


}
