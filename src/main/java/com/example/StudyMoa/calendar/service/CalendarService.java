package com.example.StudyMoa.calendar.service;

import java.util.Calendar;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import com.example.StudyMoa.common.dto.Study;

public interface CalendarService {


	List<Map<String, Object>> selectCalendarList(Map<String, Object> myschedule);
		
	boolean insertCalendar(HashMap<String, Object> schedule);

	
	List<Map<String, Object>> selectCalendarListTest(Map<String, Object> myschedule);

	
	// 삭제
	
	boolean deleteCalendar(Map<String, Object> schedule);

	// 수정 
	boolean updateCalendar(Map<String, Object> schedule);
	 

	}


