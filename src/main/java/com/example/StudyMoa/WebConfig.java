package com.example.StudyMoa;


import java.nio.file.Path;
import java.nio.file.Paths;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration()
@MapperScan(value= {"com.example.StudyMoa.login.dao"})
public class WebConfig implements WebMvcConfigurer {
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//registry.addResourceHandler("/imgUpload/**").addResourceLocations("/resources/static/imgUpload");
		//registry.addResourceHandler("/img/**").addResourceLocations("/resources/static/img");
		//addResourceHandler 를 따로 설정했을 때 이미지를 오히려 못 불러옴
	}


	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("UTF-8"); // 파일 인코딩 설정
		multipartResolver.setMaxUploadSizePerFile(5 * 1024 * 1024); // 파일당 업로드 크기 제한 (5MB)
		return multipartResolver;
	}
	
	
	@Bean 
	public String uploadPath(){ 
		
				
		String uploadtPath = "C:\\Users\\Hansol\\git\\hansolstudyproject\\src\\main\\resources\\static";

		
		return uploadtPath;
	}
	
		
}

