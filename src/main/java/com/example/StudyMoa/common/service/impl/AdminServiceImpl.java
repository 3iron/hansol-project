package com.example.StudyMoa.common.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dao.AdminMapper;
import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.service.AdminService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AdminServiceImpl implements AdminService{
	
	
	@Autowired
	AdminMapper adminMapper;

	@Override
	public boolean insertCategory(Category category) {
		
		return adminMapper.insertCategory(category);
	}

	@Override
	public List<Map<String, Object>> selectInsertUserChart() {
		
		return adminMapper.selectInsertUserChart();
	}

	@Override
	public List<Map<String, Object>> selectCategoryDistribution() {
	
		return adminMapper.selectCategoryDistribution();
	}

	@Override
	public List<Map<String, Object>> selectUserInfo() {
	
		return adminMapper.selectUserInfo();
	}

	@Override
	public boolean deleteUser(HashMap<String, Object> dataParam) {
		
		return adminMapper.deleteUser(dataParam);
	}

	@Override
	public List<Map<String, Object>> selectCategoryAdmin() {
		
		return adminMapper.selectCategoryAdmin();
	}

	@Override
	public boolean deleteCategory(HashMap<String, Object> dataParam) {
		
		return adminMapper.deleteCategory(dataParam);
	}
	
	
	
}
