package com.example.StudyMoa.common.service;

import java.util.List;

import com.example.StudyMoa.common.dto.Category;

public interface CategoryService {

	List<Category> selectCategoryList() throws Exception;

	Category selectCategoryOne(int categoryNo) throws Exception;

	boolean insertCategory(Category category);

}
