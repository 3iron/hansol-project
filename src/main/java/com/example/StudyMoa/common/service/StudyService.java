package com.example.StudyMoa.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;

public interface StudyService {
	
	List<Study> selectStudyList(Map<String, Object> map) throws Exception;				//스터디 리스트 조회

	List<Study> selectStudyList(int categoryNo) throws Exception;						//스터디 리스트 조회(카테고리 필터링 후)

	int selectStudyCount(Map<String, Object> map) throws Exception;						//스터디 총 갯수 조회
			
	Study selectStudyOne(Map<String, Object> map) throws Exception;						//스터디 단건 조회

	boolean updateStudySNow(int sNow);													//스터디 현재인원 업데이트 (스터디 신청 시)

	boolean updateStudySEndDate(HashMap<String, Object> reviewParam);					//스터디 종료일자 업데이트 (스터디장 스터디 종료 시)

	Double selectStudyOneGrade(Map<String, Object> map);								//스터디 평균 평점 조회

	boolean deleteStudy(HashMap<String, Object> paramData);								//스터디 삭제

}
