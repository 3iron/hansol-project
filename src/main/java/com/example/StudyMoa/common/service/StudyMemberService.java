package com.example.StudyMoa.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface StudyMemberService {

	boolean insertStudyMember(Map<String, Object> map);							//스터디 멤버 생성

	Map<String, Object> selectStudyMember(Map<String, Object> map);				//스터디 멤버 조회

	List<Map> selectStudyMemberList(HashMap<String, Object> paramData);			//스터디 멤버 리스트 조회

	boolean deleteStudyMember(HashMap<String, Object> paramData);				//스터디 삭제

	boolean updateExitStudyMember(HashMap<String, Object> paramData);

	
}
