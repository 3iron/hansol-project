package com.example.StudyMoa.common.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Category {
	
	
	private int categoryNo;
	private String categoryName;
	private String categoryThumbImg;
	private int categoryRemoveYn;
	
	
	@Override
	public String toString() {
		return "Category [categoryNo=" + categoryNo + ", categoryName=" + categoryName + ", categoryThumbImg="
				+ categoryThumbImg + ", categoryRemoveYn=" + categoryRemoveYn + "]";
	}
		
	

}
