package com.example.StudyMoa.common.dto;


import org.springframework.web.util.UriComponentsBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

/*페이징 처리 위한 DTO*/
public class Paging {
	
	/*현재 페이지 번호*/
	private int nowPage;
	
	/*시작페이지*/
	private int startPage;
	
	/*끝페이지*/
	private int endPage;
	
	/*게시글 총 갯수*/
	private int total;
	
	/*페이지당 글 갯수 갯수*/
	private int cntPerPage;
	
	/*마지막 페이지*/
	private int lastPage;
	
	/*sql쿼리 사용할 start*/
	private int start;
	
	/*sql쿼리 사용할 end*/
	private int end;
	
	private int cntPage = 5;
	
	public Paging(int total, int nowPage, int cntPerPage){
			setNowPage(nowPage);
			setCntPerPage(cntPerPage);
			setTotal(total);
			calcLastPage(getTotal(), getCntPerPage());
			calcStartEndPage(getNowPage(), cntPage);
			calcStartEnd(getNowPage(), getCntPerPage());
			
	}
	
	//제일 마지막 페이지 계산
	public void calcLastPage(int total, int cntPerPage){
		setLastPage((int)Math.ceil((double) total / (double) cntPerPage));
	}
	
	
	//시작, 끝 페이지 계산
	public void calcStartEndPage(int nowPage, int cntPage){
		setEndPage((int)Math.ceil((double)nowPage / (double)cntPage) * cntPage);
		if(getLastPage() < getEndPage()){
			setEndPage(getLastPage());
		}
		setStartPage(getEndPage() - cntPage + 1);
		if(getStartPage() < 1){
			setStartPage(1);
		}
	}
	
	//DB 쿼리에서 사용할 start, end 값 계산
	public void calcStartEnd(int nowPage, int cntPerPage){
		setEnd(nowPage * cntPerPage);
		setStart(getEnd() - cntPerPage);
	}

	@Override
	public String toString() {
		return "Paging [nowPage=" + nowPage + ", startPage=" + startPage + ", endPage=" + endPage + ", total=" + total
				+ ", cntPerPage=" + cntPerPage + ", lastPage=" + lastPage + ", start=" + start + ", end=" + end
				+ ", cntPage=" + cntPage + "]";
	}
	
	
}
