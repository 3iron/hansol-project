package com.example.StudyMoa.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.login.dto.User;

@Mapper
public interface MakeStudyMapper {

	// xml파일이 있어서 밑의 방식을 사용 (xml이 종결단계)

	boolean insertMakeStudy(Study study);

	boolean insertStudyLeader(Map<String, Object> map);
	
	// INSERT는 boolean 아니면 int형으로 확인
	// select는 class 중에서도 객체(table), 컬럼(스트링) 몇개만
	
}



