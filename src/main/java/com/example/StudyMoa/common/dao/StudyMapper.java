package com.example.StudyMoa.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;

@Mapper
public interface StudyMapper {
	List<Study> selectStudyList(Map<String, Object> map) throws Exception;

	List<Study> selectStudyList(int categoryNo) throws Exception;

	int selectStudyCount(Map<String, Object> map) throws Exception;

	Study selectStudyOne(Map<String, Object> map) throws Exception;

	boolean updateStudySNow(int sNow);

	boolean updateStudyEndDate(HashMap<String, Object> reviewParam);

	Double selectStudyOneGrade(Map<String, Object> map);

	boolean deleteStudy(HashMap<String, Object> paramData);


}
