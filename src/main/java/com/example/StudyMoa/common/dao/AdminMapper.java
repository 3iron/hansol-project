package com.example.StudyMoa.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.common.dto.Category;

@Mapper
public interface AdminMapper {

	boolean insertCategory(Category category);

	List<Map<String, Object>> selectInsertUserChart();

	List<Map<String, Object>> selectCategoryDistribution();

	List<Map<String, Object>> selectUserInfo();

	boolean deleteUser(HashMap<String, Object> dataParam);

	List<Map<String, Object>> selectCategoryAdmin();

	boolean deleteCategory(HashMap<String, Object> dataParam);

}
