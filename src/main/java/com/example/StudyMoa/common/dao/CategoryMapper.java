package com.example.StudyMoa.common.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.common.dto.Category;

@Mapper
public interface CategoryMapper {
	
	List<Category> selectCategoryList() throws Exception;

	Category selectCategoryOne(int categoryNo) throws Exception;

	boolean insertCategory(Category category);
}
