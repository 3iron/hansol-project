package com.example.StudyMoa.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

//import com.example.StudyMoa.login.service.UserDetailsServiceImpl;

import lombok.RequiredArgsConstructor;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


	@Override
	public void configure(WebSecurity web) throws Exception {
		// static 디렉터리의 하위 파일 목록은 인증 무시 ( = 항상통과 )
		web.ignoring().antMatchers("/css/**", "/js/**", "/img/**", "/imgUpload/**");
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.authorizeRequests() 
			//.antMatchers("/admin/**").hasAnyAuthority("ADMIN")// 관리자페이지를 위한 곳
			.antMatchers("/login","/join","/joinForm","/logout","/userIdCheck","/findid","/findidForm","/findpw","/findpwForm","/checkvalidid","/checkvalidpwd","/useredit","/usereditForm","/checkeditPwd","/updateNewPwd","/deleteuser").permitAll() 
			.anyRequest().authenticated()		//로그인된 사용자가 요청을 수행할 떄 필요하다  만약 사용자가 인증되지 않았다면, 스프링 시큐리티 필터는 요청을 잡아내고 사용자를 로그인 페이지로 리다이렉션 해준다.
			
		.and()
			//.csrf().ignoringAntMatchers("/admin/goods/ckUpload") //기본적으로 springSecurity에선 post로 controller로 정보를 보내줄떼 csrf라는 토큰이 필요하다 이것을 무시하기위한 경로
		//.and()
//			.formLogin()
//			.loginPage("/signup")
//			.failureUrl("/loginErrorPage") /*로그인 실패시 url*/ 
//			.defaultSuccessUrl("/signup", true) /*로그인 성공시 url*/
	        .formLogin()
				.loginPage("/login")
				.loginProcessingUrl("/login")
				.usernameParameter("username")
                .passwordParameter("password")
				.defaultSuccessUrl("/mainPagesuccess")
				.successForwardUrl("/mainPagesuccess")
				.failureUrl("/loginErrorPage")				//alert창만 띄워주기
			.permitAll()
		.and()
			.logout()
				.logoutUrl("/logout")
				.deleteCookies("JSESSIONID")
				.invalidateHttpSession(true)
				.logoutSuccessUrl("/login")
		.and()
			.exceptionHandling();
			//.accessDeniedPage("/accessDenied");  권한이 없는 대상이 접속을시도했을 때
	}

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) {
//		auth.authenticationProvider(authenticationProvider(securityService));
//	}
}
