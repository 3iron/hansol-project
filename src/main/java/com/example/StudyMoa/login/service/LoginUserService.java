package com.example.StudyMoa.login.service;


import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.Errors;

import com.example.StudyMoa.login.dto.User;

public interface LoginUserService {

	boolean insertUser(User user);

//	Map<String, String> validateHandling(Errors errors);

	int userIdCheck(HashMap<String, Object> paramData);				//회원가입시 유저 ID 체크 (임시)

	User LoginIdCheck(String userId, String userPwd);

	Map findUserId(HashMap<String, Object> paramData); // 단일 사용자의 ID값 하나만 사용자에게 전달해준다 => String
	
	Map findUserPwd(HashMap<String, Object> paramData); //단일 사용자의 PWD하나만을 사용자에게 전달해준다.
	Map updateNewPwd(HashMap<String, Object> paramData);
	
	boolean checkvalidid(HashMap<String, Object> paramData);

	boolean checkvalidpwd(HashMap<String, Object> paramData);

//	boolean useredit(User user);
	boolean useredit(HashMap<String, Object> paramData);

	boolean checkeditPwd(HashMap<String, Object> paramData);	//회원정보 수정시 기존의 비밀번호 확인용

	boolean deleteuser(HashMap<String, Object> paramData);

	Map selectNewPwd(HashMap<String, Object> paramData);

	

}
