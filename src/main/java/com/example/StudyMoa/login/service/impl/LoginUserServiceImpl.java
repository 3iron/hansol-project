package com.example.StudyMoa.login.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.example.StudyMoa.login.dao.LoginUserMapper;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.login.service.LoginUserService;


@Service
public class LoginUserServiceImpl implements LoginUserService{

	
//	@Autowired PasswordEncoder passwordEncoder;
	 
	@Autowired
	LoginUserMapper loginUserMapper;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	
	@Override
	public boolean insertUser(User user) {				//회원가입
		
	
		
		String encodedPassword = passwordEncoder.encode(user.getUserPwd());
		user.setUserPwd(encodedPassword);
		
		System.out.println(encodedPassword);
		
		System.out.println("서비스임플의 유저정보(DB들어갈때) :"+user);
		return loginUserMapper.insertUser(user);
		

	}


	@Override
	public Map findUserPwd(HashMap<String, Object> paramData) {
		// TODO Auto-generated method stub
		Map userPwd = loginUserMapper.findUserPwd(paramData);
		return userPwd;
	}

//	@Override //해쉬맵에 키와 에러메세지를 담아주기
//	public Map<String, String> validateHandling(Errors errors) {
//		Map<String,String> validatorResult = new HashMap<>();
//		
//		for(FieldError error : errors.getFieldErrors()) {
//			String validKeyName = String.format("valid_%s", error.getField());
//			validatorResult.put(validKeyName, error.getDefaultMessage());
//		}
//		
//		return validatorResult;
//		
//	}

	//회원가입시 아이디 체크
	@Override
	public int userIdCheck(HashMap<String, Object> paramData) {
		
		System.out.println("서비스임플 유저아이디 체크 확인");
		return loginUserMapper.userIdCheck(paramData);
	}

	@Override
	public User LoginIdCheck(String userId, String userPwd) {
		System.out.println("로그인시 아이디검사");
		
		return loginUserMapper.LoginIdCheck(userId, userPwd);
	}

	@Override
	public Map findUserId(HashMap<String, Object> paramData) {
		
		return loginUserMapper.findUserId(paramData);
	}


	@Override
	public boolean checkvalidid(HashMap<String, Object> paramData) {
		
		System.out.println("아이디 유효성 검사 impl단 진입");
		String userCheck = "^[A-Za-z0-9_\\.\\-]+@[A-Za-z0-9\\-]+\\.[A-Za-z0-9\\-]+$";
		String userId = (String) paramData.get("userId");
				
		return Pattern.matches(userCheck, userId);

		
		
	}


	@Override
	public boolean checkvalidpwd(HashMap<String, Object> paramData) {
		
		System.out.println("비밀번호 유효성 검사 impl단 진입");
		String userCheck = "^(?=.*[a-zA-Z]).{4,20}$";
		
		String userPwd = (String) paramData.get("userPwd");
		
		return Pattern.matches(userCheck, userPwd);
	}


	/*
	 * @Override public boolean useredit(HashMap<String, Object> paramData) {
	 * System.out.println("회원정보수정 impl진입");
	 * 
	 * boolean result = loginUserMapper.useredit(paramData); return result;
	 * 
	 * }
	 */


	@Override 
	public boolean useredit(HashMap<String, Object> paramData) {
	
		System.out.println("회원정보수정 impl진입");
	
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		//암호화된 내 새로운 패스워드
		String userEncChangePwd = encoder.encode((String) paramData.get("userChangePwd"));
		//암호화 패스워드 파람데이터에 넣어주기
		paramData.put("userEncChangePwd", userEncChangePwd);
		boolean result = loginUserMapper.useredit(paramData);
		
		
		System.out.println("useredit impl단 리턴값: "+result); 
		return result;
		
	
	}
	 
	

	@Override
	public boolean checkeditPwd(HashMap<String, Object> paramData) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println("회원정보수정 비밀번호비교 impl진입");
		
		User user = loginUserMapper.checkeditPwd(paramData);
		System.out.println("user :"+user);
		
		//회원의 원래비번
		String userOrgPwd = user.getUserPwd();						//DB로부터 들고온 비밀번호
		System.out.println("userOrgPwd :"+userOrgPwd);
		
		//회원이 보낸 비밀번호
		String userORGinsertPwd = (String) paramData.get("userinsertPwd");		//암호화전 회원이 보낸 패스워드
		String userinsertPwd = encoder.encode(userORGinsertPwd);				//회원이 보낸 암호화 패스워드
		System.out.println("userinsertPwd :"+userinsertPwd);
		

		if(userOrgPwd.equals(userinsertPwd)) {						//비교했을때 맞지 않으면
			System.out.println("같다");
			return true;
		}else {
			System.out.println("다르다");
			return false;
		}
		
		
	}


	@Override		//임시 비밀번호 발금
	public Map<String, Object> updateNewPwd(HashMap<String, Object> paramData) {
		
		Random random = new Random();
		//암호화하기전 임시 패스워드
		String updateNewPwd0 = random.ints(97, 123).limit(5).toString();
		updateNewPwd0 = updateNewPwd0.replaceAll("java.util.stream.SliceOps", "");
		System.out.println("임시 비밀번호 :"+updateNewPwd0);
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String updateNewPwd = encoder.encode(updateNewPwd0);			//암호화된 임시 비밀번호
		
		paramData.put("updateNewPwd", updateNewPwd);
		boolean result = loginUserMapper.updateNewPwd(paramData);
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(result = true) {												//업데이트 	
			map.put("updateNewPwd0", updateNewPwd0);
			return map;
		}else {
			map.put("updateNewPwd0", null);
			return map;
		}
		
	}


	@Override
	public boolean deleteuser(HashMap<String, Object> paramData) {
		System.out.println("딜리트 유저 들어옴");
		
		boolean deleteuserResult = loginUserMapper.deleteuser(paramData);
		if(deleteuserResult = true) {
			System.out.println("유저정보 삭제성공");
		}else {
			System.out.println("유저정보 삭제실패");
		}
		return deleteuserResult;
	}


	@Override
	public Map selectNewPwd(HashMap<String, Object> paramData) {
		System.out.println("selectNewPwd Mapper로 들어옴");
		
		System.out.println(loginUserMapper.selectNewPwd(paramData));
		
		
		return loginUserMapper.selectNewPwd(paramData);
	}
	
	



	

}
