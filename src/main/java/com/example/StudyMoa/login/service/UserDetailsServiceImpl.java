package com.example.StudyMoa.login.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.StudyMoa.login.dao.LoginUserMapper;
import com.example.StudyMoa.login.dao.UserAuthDAO;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.login.dto.UserInfoDTO;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;

	
//	@Autowired
//	private LoginUserMapper homeMapper;
//
//	@Autowired
//	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	//DB에서 유저정보를 불러온다. Custom한 Userdetails 클래스를 리턴 해주면 된다.(실질적인 로그인코드)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("서비스 id : "+username);
		
		//ArrayList<User> userAuthes = homeMapper.findByUserId(id);
		UserAuthDAO dao = sqlSession.getMapper(UserAuthDAO.class);
		//System.out.println("userAuthes size : "+userAuthes.size());
		User userDetailsDto = dao.selectUserInfo(username);
		
		if(userDetailsDto == null) {		//user을 찾지 못한경우
			System.out.println("못찾음 : "+ userDetailsDto);
			throw new UsernameNotFoundException("User "+username+" Not Found!");
		}
		System.out.println("찾음 : "+ userDetailsDto);
		return userDetailsDto;
	}
	



}