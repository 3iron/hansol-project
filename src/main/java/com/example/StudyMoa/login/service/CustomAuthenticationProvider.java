package com.example.StudyMoa.login.service;



import lombok.AllArgsConstructor;

import java.io.Console;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
//import com.example.StudyMoa.login.security.UserDetailsVO;
//import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.login.dto.User;


@Service
@AllArgsConstructor
//@Log4j2
public class CustomAuthenticationProvider implements AuthenticationProvider {
	

	

	@Autowired
    UserDetailsService userDetailsService;
	//UserDetailsServiceImpl userDetailsService;
    //private final PasswordEncoder passwordEncoder;

    @SuppressWarnings("unchecked")
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        
    	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();    	
    	//UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        
    	System.out.println("provider 실행");
    	
    	 // AuthenticaionFilter에서 생성된 토큰으로부터 아이디와 비밀번호를 조회함
        String username = (String) authentication.getPrincipal();
        //String userId = token.getName();
        String password = (String) authentication.getCredentials();
        //String userPwd = token.getCredentials();
        
        
        System.out.println("userID는? : "+username);
        //System.out.println(userPwd);
        
        // UserDetailsService를 통해 DB에서 아이디로 사용자 조회
        User user = (User) userDetailsService.loadUserByUsername(username);		//들어가는 부분
        
//        String user_Name = user.getUserName();
        

        System.out.println("들어가기전 pwd :"+password+" 자료형 :"+password.getClass().getName());
        System.out.println("들어간 후 PWD :"+user.getPassword()+" 자료형 :"+user.getPassword().getClass().getName());
        
        String DBPwd = user.getPassword();
        
        System.out.println("true?false? :"+(password.equals(DBPwd)));
      if (!encoder.matches(password, user.getPassword())) {        
        //문제발생
        //if (!userPwd.equals(DBPwd)) {
        	System.out.println("user는 : "+ user);
        	System.out.println("Password1는 : "+password);
        	System.out.println("Password1는 : "+user.getPassword());
            throw new BadCredentialsException(user.getUsername() + "계정의 패스워드가 알맞지 않습니다.");		//userInfoDTO정의 필요
    	
        }	
        	System.out.println("Password2는 : "+password);
        	System.out.println("Password2는 : "+user.getPassword());
            //return new UsernamePasswordAuthenticationToken(userDetailsVO, userPwd, userDetailsVO.getAuthorities());
                
        
    	
//        if(!user.isEnabled()) {
//            throw new BadCredentialsException(userId);
//        }	
        
	        System.out.print("Login Password : ");
	        System.out.println(password);
	        System.out.print("Login SUCCESS");

	        //UserDetails 객체를 생성자로 제공해주는게 좋습니다.
	        UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
	        System.out.println(result);
	        
	        return result;
        
        
        }
    
    

    @Override
    public boolean supports(Class<?> authentication) {
        //return authentication.equals(UsernamePasswordAuthenticationToken.class);
    	return true;
    }
}
